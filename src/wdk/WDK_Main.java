/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package wdk;

import java.io.IOException;
import java.util.ArrayList;
import static wdk.WDK_StartupConstants.*;
import static wdk.WDK_PropertyType.*;
import java.util.Locale;
import javafx.application.Application;
import static javafx.application.Application.launch;
import javafx.stage.Stage;
import properties_manager.PropertiesManager;
import wdk.gui.WDK_UI;
import static wdk.WDK_StartupConstants.*;
import wdk.data.DraftDataManager;
import wdk.file.JsonFileManager;
import xml_utilities.InvalidXMLFileFormatException;

/**
 *
 * @author Thierno
 */
public class WDK_Main extends Application {
    WDK_UI gui;

    /**
     * This is where our Application begins its initialization, it will
     * create the GUI and initialize all of its components.
     * 
     * @param primaryStage This application's window.
     */
    @Override
    public void start(Stage primaryStage) {
        // LET'S START BY GIVING THE PRIMARY STAGE TO OUR ERROR HANDLER
        /*ErrorHandler eH = ErrorHandler.getErrorHandler();
        eH.initMessageDialog(primaryStage);
        
        // LOAD APP SETTINGS INTO THE GUI AND START IT UP
        */
        boolean success = loadProperties();
        if (success) {
            PropertiesManager props = PropertiesManager.getPropertiesManager();
            String appTitle = props.getProperty(PROP_APP_TITLE);
          // try {                
                // WE WILL SAVE OUR COURSE DATA USING THE JSON FILE
                // FORMAT SO WE'LL LET THIS OBJECT DO THIS FOR US
                JsonFileManager jsonFileManager = new JsonFileManager();
                
                // AND THIS ONE WILL DO THE COURSE WEB PAGE EXPORTING
               /* CourseSiteExporter exporter = new CourseSiteExporter(PATH_BASE, PATH_SITES);
                
                Instructor lastInstructor = jsonFileManager.loadLastInstructor(JSON_FILE_PATH_LAST_INSTRUCTOR);
                ArrayList<String> subjects = jsonFileManager.loadSubjects(JSON_FILE_PATH_SUBJECTS);
                 */               
                // AND NOW GIVE ALL OF THIS STUFF TO THE GUI
                // INITIALIZE THE USER INTERFACE COMPONENTS
       
                
                gui = new WDK_UI(primaryStage);
                DraftDataManager dataManager = new DraftDataManager(gui); 
                gui.setDataManager(dataManager);
                gui.setDraftFileManager(jsonFileManager);
               // gui.setSiteExporter(exporter);
                
                // CONSTRUCT THE DATA MANAGER AND GIVE IT TO THE GUI
                //DraftDataManager dataManager = new DraftDataManager(gui); 
                //gui.setDataManager(dataManager);

                // FINALLY, START UP THE USER INTERFACE WINDOW AFTER ALL
                // REMAINING INITIALIZATION
               
                //gui.initGUI(appTitle, subjects);   
               
                gui.initGUI(appTitle);
            }
          /*  catch(IOException ioe) {
               // eH = ErrorHandler.getErrorHandler();
               // eH.handlePropertiesFileError();
            }*/
        }
    
    
    /**
     * Loads this application's properties file, which has a number of settings
     * for initializing the user interface.
     * 
     * @return true if the properties file was loaded successfully, false otherwise.
     */
    public boolean loadProperties() {
        try {
            // LOAD THE SETTINGS FOR STARTING THE APP
            PropertiesManager props = PropertiesManager.getPropertiesManager();
            props.addProperty(PropertiesManager.DATA_PATH_PROPERTY, PATH_DATA);
            props.loadProperties(PROPERTIES_FILE_NAME, PROPERTIES_SCHEMA_FILE_NAME);
            return true;
       } catch (InvalidXMLFileFormatException ixmlffe) {
            // SOMETHING WENT WRONG INITIALIZING THE XML FILE
           // ErrorHandler eH = ErrorHandler.getErrorHandler();
           // eH.handlePropertiesFileError();
            return false;
        }       
    }

    /**
     * This is where program execution begins. Since this is a JavaFX app
     * it will simply call launch, which gets JavaFX rolling, resulting in
     * sending the properly initialized Stage (i.e. window) to our start
     * method in this class.
     */
    public static void main(String[] args) {
        Locale.setDefault(Locale.US);
        launch(args);
    }
    
}
