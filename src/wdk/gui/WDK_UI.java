/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package wdk.gui;

import java.util.List;
import java.util.concurrent.locks.ReentrantLock;
import java.util.logging.Level;
import java.util.logging.Logger;
import javafx.application.Platform;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.concurrent.Task;
import static wdk.WDK_StartupConstants.*;
import wdk.WDK_PropertyType;
import javafx.geometry.Rectangle2D;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.ScrollPane;
import javafx.scene.control.Tooltip;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.FlowPane;
import javafx.scene.layout.Pane;
import javafx.scene.layout.VBox;
import javafx.stage.Screen;
import javafx.stage.Stage;
import properties_manager.PropertiesManager;
import wdk.controller.FileController;
import wdk.data.Draft;
import wdk.data.DraftDataManager;
import wdk.data.DraftDataView;
import wdk.file.DraftFileManager;
import wdk.file.DraftSiteExporter;
import javafx.scene.control.ComboBox;
import javafx.scene.control.ProgressBar;
import javafx.scene.control.ProgressIndicator;
import javafx.scene.control.RadioButton;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.TextField;
import javafx.scene.control.ToggleGroup;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.layout.HBox;
import wdk.controller.DraftEditController;
import wdk.controller.PlayerEditController;
import wdk.data.FantasyTeam;
import wdk.data.PlayerPresentor;

/**
 *
 * @author Thierno
 */
public class WDK_UI implements DraftDataView{
    
    static final String PRIMARY_STYLE_SHEET = PATH_CSS + "wdk_style.css";
    static final String CLASS_BORDERED_PANE = "bordered_pane";
    static final String CLASS_BORDERED_PANE2 = "bordered_pane2";
    static final String CLASS_SUBJECT_PANE = "subject_pane";
    static final String CLASS_HEADING_LABEL = "heading_label";
    static final String CLASS_SUBHEADING_LABEL = "subheading_label";
    static final String CLASS_PROMPT_LABEL = "prompt_label";
    static final String EMPTY_TEXT = "";
    static final int LARGE_TEXT_FIELD_LENGTH = 1100;
    static final int SMALL_TEXT_FIELD_LENGTH = 5;
    static final int MEDIUM_TEXT_FIELD_LENGTH = 240;
    private final Stage primaryStage;
    private FlowPane fileToolbarPane;
    private Button newDraftButton;
    private Button loadDraftButton;
    private Button saveDraftButton;
    private Button exportSiteButton;
    private Button exitButton;
    private BorderPane workspacePane;
    
    private ScrollPane workspaceScrollPane;
    private BorderPane wdkPane;
    private Parent csbPane;
    private Scene primaryScene;
    private boolean workspaceActivated;
    private Pane fantasyTeamPane;
    
    
    DraftDataManager dataManager;

    // THIS MANAGES COURSE FILE I/O
    DraftFileManager draftFileManager;

    // THIS MANAGES EXPORTING OUR SITE PAGES
    DraftSiteExporter siteExporter;
    private FileController fileController;
    
    MessageDialog messageDialog;
    YesNoCancelDialog yesNoCancelDialog;
    private FlowPane screenSelectionToolbarPane;
    private Button homeScreenButton;
    private Button playerScreenButton;
    private Button standingScreenButton;
    private Button draftScreenButton;
    private Button MLBScreenButton;
    private Label fantasyTeamLabel;
    private VBox homeScreenPane;
    private Label homeScreenHeadingLabel;
    private VBox draftScreenPane;
    private Label draftScreenHeadingLabel;
    private VBox MLBScreenPane;
    private Label MLBScreenHeadingLabel;
    private Pane playerScreenPane;
    private Label playerScreenHeadingLabel;
    private VBox playerScreenBox;
    private HBox playerItemsToolbar;
    private Button addPlayerButton;
    private Button removePlayerButton;
    private HBox searchBox;
    private Label searchLabel;
    private TextField searchField;
    private FlowPane topToolbar;
    private RadioButton allRadioButton;
    private RadioButton catcherRadioButton;
    private RadioButton firstBaseRadioButton;
    private RadioButton secondBaseRadioButton;
    private RadioButton thirdBaseRadioButton;
    private RadioButton cornerInfRadioButton;
    private RadioButton middleInfRadioButton;
    private RadioButton shortStopRadioButton;
    private RadioButton outFieldInfRadioButton;
    private RadioButton utilityRadioButton;
    private RadioButton pRadioButton;
    private HBox radioButtonsBox;
    private TableView playerTable;
    
    //COLUMNS CONSTANTS 
    static final String COL_LAST = "Last";
    static final String COL_FIRST = "First";
    static final String COL_TEAM = "Pro Team";
    static final String COL_FANTASY_TEAM = "Team";
    static final String COL_POSITIONS = "Positions";
    static final String COL_POSITION = "Position";
    static final String COL_ORDER = "Pick #";
    static final String COL_BIRTH_YEAR = "Year of Birth";
    static final String COL_R_W = "R/W";
    static final String COL_HR_SV = "HR/SV";
    static final String COL_RBI_K = "RBI/K";
    static final String COL_SB_ERA = "SB/ERA";
    static final String COL_BA_WHIP = "BA/WHIP";
    static final String COL_VALUE = "Estimated Value";
    static final String COL_NOTE = "Notes";
    private TableColumn topicColumn;
    private TableColumn lastColumn;
    private TableColumn firstColumn;
    private TableColumn teamColumn;
    private TableColumn positionColumn;
    private TableColumn birthColumn;
    private TableColumn r_wColumn;
    private TableColumn hr_svColumn;
    private TableColumn rbi_kColumn;
    private TableColumn sb_eraColumn;
    private TableColumn ba_whipColumn;
    private TableColumn valueColumn;
    private TableColumn noteColumn;
    private ToggleGroup group;
    private ObservableList<PlayerPresentor> playersChange;
    private HBox draftNameBox;
    private Label draftNameLabel;
    private TextField draftNameField;
    private HBox draftItemsToolbar;
    private Button addTeamButton;
    private Button removeTeamButton;
    private Button editTeamButton;
    private HBox draftButtonToolbar;
    private ComboBox fantasyTeamComboBox;
    private VBox lineUpTableBox;
    private TableView lineUpTable;
    private TableColumn lineUpLastColumn;
    private TableColumn lineUpFirstColumn;
    private TableColumn lineUpTeamColumn;
    private TableColumn lineUpPositionProColumn;
    private TableColumn lineUpBirthColumn;
    private TableColumn lineUpr_wColumn;
    private TableColumn lineUphr_svColumn;
    private TableColumn lineUprbi_kColumn;
    private TableColumn lineUpsb_eraColumn;
    private TableColumn lineUpba_whipColumn;
    private TableColumn lineUpvalueColumn;
    private TableColumn lineUpnoteColumn;
    private TableColumn lineUpPositionColumn;
    private Label lineUpTableLabel;
    private VBox taxiSquadTableBox;
    private Label taxiSquadTableLabel;
    private TableView taxiSquadTable;
    private TableColumn taxiSquadPositionColumn;
    private TableColumn taxiSquadLastColumn;
    private TableColumn taxiSquadFirstColumn;
    private TableColumn taxiSquadTeamColumn;
    private TableColumn taxiSquadPositionProColumn;
    private TableColumn taxiSquadBirthColumn;
    private TableColumn taxiSquadr_wColumn;
    private TableColumn taxiSquadhr_svColumn;
    private TableColumn taxiSquadrbi_kColumn;
    private TableColumn taxiSquadsb_eraColumn;
    private TableColumn taxiSquadba_whipColumn;
    private TableColumn taxiSquadvalueColumn;
    private TableColumn taxiSquadnoteColumn;
    private DraftEditController draftEditController;
    private TableColumn lineUpSalaryColumn;
    private TableColumn lineUpContractColumn;
    private TableColumn taxiSquadContractColumn;
    private TableColumn taxiSquadSalaryColumn;
    private HBox proTeamsToolbar;
    private ComboBox proTeamsCombox;
    private Label proTeamLabel;
    private final String PROTEAM_PROMPT = "Slect Pro Team: ";
    private TableView proTeamsPlayerTable;
    private TableColumn proTeamsPositionsCol;
    private TableColumn proTeamsLastColumn;
    private TableColumn proTeamsFirstColumn;
    private HBox sumaryButtonToolbar;
    private Button selectPlayerButton;
    private Button autoDraftButton;
    private Button pauseDraftButton;
    private TableView sumaryPlayerTable;
    private TableColumn sumaryPlayerLastColumn;
    private TableColumn sumaryPlayerFirstColumn;
    private TableColumn sumaryPlayerTeamCol;
    private TableColumn orderSumaryPlayerColumn;
    private TableColumn sumaryPlayerContractColumn;
    private TableColumn sumaryPlayerSalaryColumn;
    private VBox antasyStandingTableBox;
    private VBox fantasyStandingTableBox;
    private TableView fantasyStandingTable;
    private TableColumn fantasyStandingTeamNameColumn;
    private TableColumn fantasyStandingPlayerNeededColumn;
    private TableColumn fantasyStandingLeftMoneyColumn;
    private TableColumn fantasyStandingPPColumn;
    private TableColumn fantasyStandingRColumn;
    private TableColumn fantasyStandingHRColumn;
    private TableColumn fantasyStandingRIBColumn;
    private TableColumn fantasyStandingSBColumn;
    private TableColumn fantasyStandingBAColumn;
    private TableColumn fantasyStandingWColumn;
    private TableColumn fantasyStandingSVColumn;
    private TableColumn fantasyStandingKColumn;
    private TableColumn fantasyStandingERAColumn;
    private TableColumn fantasyStandingWHIPColumn;
    private TableColumn fantasyStandingTotalPointsColumn;
    ObservableList<PlayerPresentor> playersOfSummaryTab ;
    int pickOrder = 0;
    int playerIndex = 0, teamIndex = 0;
    private ReentrantLock myLock;
    boolean isPause = false;
    boolean lineupsFull = false;
    boolean isThreadRunning = false;
    public WDK_UI(Stage initPrimaryStage) {
        primaryStage = initPrimaryStage;
    }
    public FileController getFileController(){
        return fileController;
    }

    public void initGUI(String appTitle) {
        initDialogs();
        initFileToolbar();
        
        initWorkspace();
        initEventHandlers();
        initWindow(appTitle);      
    }
    public void start() throws Exception {
        myLock = new ReentrantLock();
        Task<Void> task = new Task<Void>() {

            double max = 10000;
            double perc;
            @Override
            protected Void call() throws Exception {
                try {
                    myLock.lock();
                for (int i = 0; i <= max; i++) {
                    //System.out.println(i);
                    perc = i/max;

                    // THIS WILL BE DONE ASYNCHRONOUSLY VIA MULTITHREADING
                    Platform.runLater(new Runnable() {
                        @Override
                        public void run() {
                            if(!isPause){
                              //if(!lineupsFull)  
                                initAutoDraft();
                             // else
                              // initAutoDraftTaxiSquad();
                            }
                        }
                    });

                    // SLEEP EACH FRAME
                    //try {
                        Thread.sleep(100);
                   // } catch (InterruptedException ie) {
                  //      ie.printStackTrace();
                  //  }
                }}
                finally {
                    myLock.unlock();
                    }
                return null;
            }
        };
        // THIS GETS THE THREAD ROLLING
        
        Thread thread = new Thread(task);
        thread.setDaemon(true);
        thread.start();            
        //});        
        
    }
    private void initDialogs() {
        messageDialog = new MessageDialog(primaryStage, CLOSE_BUTTON_LABEL);
        yesNoCancelDialog = new YesNoCancelDialog(primaryStage);
    }
    private void initFileToolbar() {
        fileToolbarPane = new FlowPane();

        // HERE ARE OUR FILE TOOLBAR BUTTONS, NOTE THAT SOME WILL
        // START AS ENABLED (false), WHILE OTHERS DISABLED (true)
        newDraftButton = initChildButton(fileToolbarPane, WDK_PropertyType.NEW_COURSE_ICON, WDK_PropertyType.NEW_COURSE_TOOLTIP, false);
        loadDraftButton = initChildButton(fileToolbarPane, WDK_PropertyType.LOAD_COURSE_ICON, WDK_PropertyType.LOAD_COURSE_TOOLTIP, false);
        saveDraftButton = initChildButton(fileToolbarPane, WDK_PropertyType.SAVE_COURSE_ICON, WDK_PropertyType.SAVE_COURSE_TOOLTIP, true);
        exportSiteButton = initChildButton(fileToolbarPane, WDK_PropertyType.EXPORT_PAGE_ICON, WDK_PropertyType.EXPORT_PAGE_TOOLTIP, true);
        exitButton = initChildButton(fileToolbarPane, WDK_PropertyType.EXIT_ICON, WDK_PropertyType.EXIT_TOOLTIP, false);
    }
    private void initScreenSelectionToolbar(){
        screenSelectionToolbarPane = new FlowPane();
        screenSelectionToolbarPane.setStyle("-fx-background-color: #cc0000");
        homeScreenButton = initChildButton(screenSelectionToolbarPane, WDK_PropertyType.HOME_ICON, WDK_PropertyType.HOME_TOOLTIP, false);
        playerScreenButton = initChildButton(screenSelectionToolbarPane, WDK_PropertyType.PLAYER_ICON, WDK_PropertyType.PLAYER_TOOLTIP, false);
        standingScreenButton = initChildButton(screenSelectionToolbarPane, WDK_PropertyType.STANDING_ICON, WDK_PropertyType.STANDING_TOOLTIP, false);
        draftScreenButton = initChildButton(screenSelectionToolbarPane, WDK_PropertyType.DRAFT_ICON, WDK_PropertyType.DRAFT_TOOLTIP, false);
        MLBScreenButton = initChildButton(screenSelectionToolbarPane, WDK_PropertyType.MLB_ICON, WDK_PropertyType.MLB_TOOLTIP, false);
    }
    private void initWorkspace(){
        
        initFantasyTeamPane();
        initScreenSelectionToolbar();
        initDraftScreen();
        initMLBScreen();
        initPalyerScreen();
        initHomeScreen();
        workspacePane = new BorderPane();
        // workspacePane.setTop(topWorkspacePane);
       // workspacePane.setCenter(schedulePane);
        workspacePane.getStyleClass().add(CLASS_BORDERED_PANE);
        workspacePane.setStyle("-fx-background-color: WHITESMOKE");
         
        workspacePane.setCenter(homeScreenPane);
        // AND NOW PUT IT IN THE WORKSPACE
        workspaceScrollPane = new ScrollPane();
        workspaceScrollPane.setContent(workspacePane);
        workspaceScrollPane.setFitToWidth(true);
       // wdkPane.setCenter(workspaceScrollPane);
        workspaceActivated = false;
    }
    private void initFantasyTeamPane(){
        // Standing
        fantasyTeamPane = new VBox();
        fantasyTeamLabel = initChildLabel(fantasyTeamPane, WDK_PropertyType.FANTASY_TEAMS_SCREEN_HEADING_LABEL, CLASS_HEADING_LABEL);
        initFantasyStandingTable();
        fantasyTeamPane.getChildren().add(fantasyStandingTableBox);
    }
    private void initFantasyStandingTable(){
        fantasyStandingTableBox = new VBox();
        fantasyStandingTableBox.getStyleClass().add(CLASS_BORDERED_PANE2);
        
        fantasyStandingTable = new TableView();
        fantasyStandingTableBox.getChildren().add(fantasyStandingTable);
        fantasyStandingTeamNameColumn = new TableColumn("Team Name");
        fantasyStandingTeamNameColumn.setMinWidth(100);
        fantasyStandingPlayerNeededColumn = new TableColumn("Player Needed");
        fantasyStandingPlayerNeededColumn.setMinWidth(100);
        fantasyStandingLeftMoneyColumn = new TableColumn("$ Left");
        fantasyStandingLeftMoneyColumn.setMinWidth(100);
        fantasyStandingPPColumn = new TableColumn("$ PP");  
        fantasyStandingPPColumn.setMinWidth(80);
        fantasyStandingRColumn = new TableColumn("R");   
        fantasyStandingRColumn.setMinWidth(50);
        fantasyStandingHRColumn = new TableColumn("HR"); 
        fantasyStandingHRColumn.setMinWidth(50);
        fantasyStandingRIBColumn = new TableColumn("RBI");
        fantasyStandingRIBColumn.setMinWidth(80);
        fantasyStandingSBColumn = new TableColumn("SB"); 
        fantasyStandingSBColumn.setMinWidth(50);
        fantasyStandingBAColumn = new TableColumn("BA");
        fantasyStandingBAColumn.setMinWidth(50);
        fantasyStandingWColumn = new TableColumn("W"); 
        fantasyStandingWColumn.setMinWidth(50);
        fantasyStandingSVColumn = new TableColumn("SV");
        fantasyStandingSVColumn.setMinWidth(50);
        //lineUpba_whipColumn.setMinWidth(100);
        fantasyStandingKColumn = new TableColumn("K"); 
        fantasyStandingKColumn.setMinWidth(50);
        fantasyStandingERAColumn= new TableColumn("ERA") ;
        fantasyStandingERAColumn.setMinWidth(100);
        fantasyStandingWHIPColumn = new TableColumn("WHIP");
        fantasyStandingWHIPColumn.setMinWidth(100);
        fantasyStandingTotalPointsColumn = new TableColumn("Total Points");
        fantasyStandingTotalPointsColumn.setMinWidth(100);
        fantasyStandingTable.getColumns().addAll(fantasyStandingTeamNameColumn,
              fantasyStandingPlayerNeededColumn,fantasyStandingLeftMoneyColumn,
              fantasyStandingPPColumn,fantasyStandingRColumn,fantasyStandingHRColumn,
              fantasyStandingRIBColumn,fantasyStandingSBColumn,fantasyStandingBAColumn,
              fantasyStandingWColumn,fantasyStandingSVColumn,fantasyStandingKColumn,
              fantasyStandingERAColumn,fantasyStandingWHIPColumn,fantasyStandingTotalPointsColumn);
    }
    public void loadFantasyStandingTable(ObservableList<FantasyTeam> teams){
        fantasyStandingTeamNameColumn.setCellValueFactory(new PropertyValueFactory<>("name"));
        fantasyStandingPlayerNeededColumn.setCellValueFactory(new PropertyValueFactory<>("playerNeeded"));
        fantasyStandingLeftMoneyColumn.setCellValueFactory(new PropertyValueFactory<FantasyTeam, Double>("budjet"));
        fantasyStandingPPColumn.setCellValueFactory(new PropertyValueFactory<FantasyTeam, Integer>("pp"));
        fantasyStandingRColumn.setCellValueFactory(new PropertyValueFactory<FantasyTeam, Integer>("r"));
        fantasyStandingHRColumn.setCellValueFactory(new PropertyValueFactory<FantasyTeam, Integer>("hr"));
        fantasyStandingRIBColumn.setCellValueFactory(new PropertyValueFactory<FantasyTeam, Integer>("rbi"));
        fantasyStandingSBColumn.setCellValueFactory(new PropertyValueFactory<FantasyTeam, Integer>("sb"));
        fantasyStandingBAColumn.setCellValueFactory(new PropertyValueFactory<FantasyTeam, Double>("ba"));
        fantasyStandingWColumn.setCellValueFactory(new PropertyValueFactory<FantasyTeam, Double>("w"));
        fantasyStandingSVColumn.setCellValueFactory(new PropertyValueFactory<FantasyTeam, Integer>("sv"));
        fantasyStandingKColumn.setCellValueFactory(new PropertyValueFactory<FantasyTeam, Integer>("k"));
        fantasyStandingERAColumn.setCellValueFactory(new PropertyValueFactory<FantasyTeam, Double>("era"));
        fantasyStandingWHIPColumn.setCellValueFactory(new PropertyValueFactory<FantasyTeam, Double>("whip"));
       
        fantasyStandingTotalPointsColumn.setCellValueFactory(new PropertyValueFactory<FantasyTeam, Double>("totalPoints"));
        
        fantasyStandingTable.setItems(teams);
        
    }
    private void initHomeScreen(){
        homeScreenPane = new VBox();
        homeScreenHeadingLabel = initChildLabel(homeScreenPane,
                WDK_PropertyType.HOME_SCREEN_HEADING_LABEL, 
                CLASS_HEADING_LABEL);
        draftNameBox = new HBox();
        draftNameLabel = initChildLabel(draftNameBox,
                WDK_PropertyType.DRAFT_NAME_LABEL, 
                CLASS_PROMPT_LABEL);
        draftNameField = new TextField();
        draftNameField.textProperty().addListener((observable, oldValue, newValue) -> {
            dataManager.getDraft().setDraftName(newValue);
            getFileController().markAsEdited(this);
        });
        draftNameField.setPrefWidth(MEDIUM_TEXT_FIELD_LENGTH);
        
        draftNameBox.getChildren().add(draftNameField);
        draftNameBox.setSpacing(5.0);
        draftItemsToolbar = new HBox();
        
        draftButtonToolbar = new HBox();
        addTeamButton = initChildButton(draftButtonToolbar,
                WDK_PropertyType.ADD_ICON, WDK_PropertyType.ADD_TEAM_TOOLTIP, false);
        removeTeamButton = initChildButton(draftButtonToolbar,
                WDK_PropertyType.MINUS_ICON, WDK_PropertyType.REMOVE_TEAM_TOOLTIP, false);
        editTeamButton = initChildButton(draftButtonToolbar,
                WDK_PropertyType.EDIT_ICON, WDK_PropertyType.EDIT_DRAFT_TOOLTIP, false);
       draftItemsToolbar.getChildren().add(draftButtonToolbar);
        draftItemsToolbar.setSpacing(20.0);
        
        HBox fantasyTeam = new HBox();
        draftNameLabel = initChildLabel(fantasyTeam,
                WDK_PropertyType.SELECT_TEAM_LABEL, 
                CLASS_PROMPT_LABEL);
        fantasyTeamComboBox = new ComboBox();
        
        fantasyTeamComboBox.setItems(dataManager.getDraft().getFantasyTeams());
        //fantasyTeamComboBox.getItems().remove(0);
        fantasyTeam.getChildren().add(fantasyTeamComboBox);
        fantasyTeam.setSpacing(5.0);
        draftItemsToolbar.getChildren().add(fantasyTeam);
        homeScreenPane.getChildren().add(draftNameBox);
        homeScreenPane.getChildren().add(draftItemsToolbar);
        initLineUpTable();
        homeScreenPane.getChildren().add(lineUpTableBox);
        initTaxiSquadTable();
        homeScreenPane.getChildren().add(taxiSquadTableBox);
    }
    private void initLineUpTable(){
        lineUpTableBox = new VBox();
        lineUpTableBox.getStyleClass().add(CLASS_BORDERED_PANE2);
        lineUpTableLabel = initChildLabel(lineUpTableBox,
                WDK_PropertyType.STARTING_LINEUP, 
                CLASS_SUBHEADING_LABEL);
        lineUpTable = new TableView();
        lineUpTableBox.getChildren().add(lineUpTable);
        lineUpPositionColumn = new TableColumn(COL_POSITION);
        lineUpLastColumn = new TableColumn(COL_LAST);
        //lineUpLastColumn.setMinWidth(100);
        lineUpFirstColumn = new TableColumn(COL_FIRST);
        //lineUpFirstColumn.setMinWidth(100);
        lineUpTeamColumn = new TableColumn(COL_TEAM);
       // lineUpTeamColumn.setMinWidth(100);
        lineUpPositionProColumn = new TableColumn(COL_POSITIONS);
        //lineUpPositionProColumn.setMinWidth(100);
        lineUpBirthColumn = new TableColumn(COL_BIRTH_YEAR);
        //lineUpBirthColumn.setMinWidth(100);
        lineUpr_wColumn = new TableColumn(COL_R_W);
        //lineUpr_wColumn.setMinWidth(100);
        lineUphr_svColumn = new TableColumn(COL_HR_SV);
        //lineUphr_svColumn.setMinWidth(100);
        lineUprbi_kColumn = new TableColumn(COL_RBI_K);
        //lineUprbi_kColumn.setMinWidth(100);
        lineUpsb_eraColumn = new TableColumn(COL_SB_ERA);
        //lineUpsb_eraColumn.setMinWidth(100);
        lineUpba_whipColumn = new TableColumn(COL_BA_WHIP);
        //lineUpba_whipColumn.setMinWidth(100);
        lineUpvalueColumn = new TableColumn(COL_VALUE);
        
        lineUpvalueColumn.setMinWidth(100);
        lineUpContractColumn = new TableColumn("Contract");
        lineUpSalaryColumn = new TableColumn("Salary");
        lineUpnoteColumn = new TableColumn(COL_NOTE);
        lineUpnoteColumn.setEditable(true);
       // lineUpnoteColumn.setMinWidth(100);
        lineUpTable.getColumns().addAll(lineUpPositionColumn, lineUpFirstColumn,lineUpLastColumn,
               lineUpTeamColumn, lineUpPositionProColumn, lineUpBirthColumn, lineUpr_wColumn,
               lineUphr_svColumn, lineUprbi_kColumn,lineUpsb_eraColumn,lineUpba_whipColumn,
               lineUpvalueColumn, lineUpContractColumn,lineUpSalaryColumn,lineUpnoteColumn);
    }
    private void loadLineUpTable(ObservableList<PlayerPresentor> players){
        lineUpPositionColumn.setCellValueFactory(new PropertyValueFactory<PlayerPresentor, String>("assignedPosition"));
        lineUpFirstColumn.setCellValueFactory(new PropertyValueFactory<PlayerPresentor, String>("firstName"));
        lineUpLastColumn.setCellValueFactory(new PropertyValueFactory<PlayerPresentor, String>("lastName"));
        lineUpTeamColumn.setCellValueFactory(new PropertyValueFactory<PlayerPresentor, String>("team"));
        lineUpPositionProColumn.setCellValueFactory(new PropertyValueFactory<PlayerPresentor, String>("position"));
        lineUpBirthColumn.setCellValueFactory(new PropertyValueFactory<PlayerPresentor, Integer>("birthYear"));
        lineUpr_wColumn.setCellValueFactory(new PropertyValueFactory<PlayerPresentor, Integer>("rw"));
        lineUphr_svColumn.setCellValueFactory(new PropertyValueFactory<PlayerPresentor, Integer>("hrsv"));
        lineUprbi_kColumn.setCellValueFactory(new PropertyValueFactory<PlayerPresentor, Integer>("rbik"));
        lineUpsb_eraColumn.setCellValueFactory(new PropertyValueFactory<PlayerPresentor, Double>("sbera"));
        lineUpba_whipColumn.setCellValueFactory(new PropertyValueFactory<PlayerPresentor, Double>("bawhip"));
        lineUpvalueColumn.setCellValueFactory(new PropertyValueFactory<PlayerPresentor, Double>("bawhip"));
        lineUpContractColumn.setCellValueFactory(new PropertyValueFactory<PlayerPresentor, String>("contract"));
        lineUpSalaryColumn.setCellValueFactory(new PropertyValueFactory<PlayerPresentor, Double>("salary"));
       
        lineUpnoteColumn.setCellValueFactory(new PropertyValueFactory<PlayerPresentor, String>("notes"));
       
        lineUpTable.setItems(players);
    }
    
    private void initTaxiSquadTable(){
        taxiSquadTableBox = new VBox();
        taxiSquadTableBox.getStyleClass().add(CLASS_BORDERED_PANE2);
        taxiSquadTableLabel = initChildLabel(taxiSquadTableBox,
                WDK_PropertyType.TAXI_SQUAD_LABEL, 
                CLASS_SUBHEADING_LABEL);
        taxiSquadTable = new TableView();
        taxiSquadTableBox.getChildren().add(taxiSquadTable);
        taxiSquadPositionColumn = new TableColumn(COL_POSITION);
        taxiSquadLastColumn = new TableColumn(COL_LAST);
        
        taxiSquadFirstColumn = new TableColumn(COL_FIRST);
       
        taxiSquadTeamColumn = new TableColumn(COL_TEAM);
       // lineUpTeamColumn.setMinWidth(100);
        taxiSquadPositionProColumn = new TableColumn(COL_POSITIONS);
        //lineUpPositionProColumn.setMinWidth(100);
        taxiSquadBirthColumn = new TableColumn(COL_BIRTH_YEAR);
        //lineUpBirthColumn.setMinWidth(100);
        taxiSquadr_wColumn = new TableColumn(COL_R_W);
        //lineUpr_wColumn.setMinWidth(100);
        taxiSquadhr_svColumn = new TableColumn(COL_HR_SV);
        //lineUphr_svColumn.setMinWidth(100);
        taxiSquadrbi_kColumn = new TableColumn(COL_RBI_K);
        //lineUprbi_kColumn.setMinWidth(100);
        taxiSquadsb_eraColumn = new TableColumn(COL_SB_ERA);
        //lineUpsb_eraColumn.setMinWidth(100);
        taxiSquadba_whipColumn = new TableColumn(COL_BA_WHIP);
        //lineUpba_whipColumn.setMinWidth(100);
        taxiSquadvalueColumn = new TableColumn(COL_VALUE);
        taxiSquadvalueColumn.setMinWidth(100);
        taxiSquadContractColumn = new TableColumn("Contract");
        taxiSquadSalaryColumn = new TableColumn("Salary");
        taxiSquadnoteColumn = new TableColumn(COL_NOTE);
       // lineUpnoteColumn.setMinWidth(100);
        taxiSquadTable.getColumns().addAll(taxiSquadPositionColumn, taxiSquadFirstColumn,taxiSquadLastColumn,
               taxiSquadTeamColumn, taxiSquadPositionProColumn, taxiSquadBirthColumn, taxiSquadr_wColumn,
               taxiSquadhr_svColumn, taxiSquadrbi_kColumn,taxiSquadsb_eraColumn,taxiSquadba_whipColumn,
               taxiSquadvalueColumn,taxiSquadContractColumn, taxiSquadSalaryColumn, taxiSquadnoteColumn);
    }
    private void loadTaxiSquadTable(ObservableList<PlayerPresentor> players){
        taxiSquadPositionColumn.setCellValueFactory(new PropertyValueFactory<PlayerPresentor, String>("assignedPosition"));
        taxiSquadFirstColumn.setCellValueFactory(new PropertyValueFactory<PlayerPresentor, String>("firstName"));
        taxiSquadLastColumn.setCellValueFactory(new PropertyValueFactory<PlayerPresentor, String>("lastName"));
        taxiSquadTeamColumn.setCellValueFactory(new PropertyValueFactory<PlayerPresentor, String>("team"));
        taxiSquadPositionProColumn.setCellValueFactory(new PropertyValueFactory<PlayerPresentor, String>("position"));
        taxiSquadBirthColumn.setCellValueFactory(new PropertyValueFactory<PlayerPresentor, Integer>("birthYear"));
        taxiSquadr_wColumn.setCellValueFactory(new PropertyValueFactory<PlayerPresentor, Integer>("rw"));
        taxiSquadhr_svColumn.setCellValueFactory(new PropertyValueFactory<PlayerPresentor, Integer>("hrsv"));
        taxiSquadrbi_kColumn.setCellValueFactory(new PropertyValueFactory<PlayerPresentor, Integer>("rbik"));
        taxiSquadsb_eraColumn.setCellValueFactory(new PropertyValueFactory<PlayerPresentor, Double>("sbera"));
        taxiSquadba_whipColumn.setCellValueFactory(new PropertyValueFactory<PlayerPresentor, Double>("bawhip"));
        taxiSquadvalueColumn.setCellValueFactory(new PropertyValueFactory<PlayerPresentor, Double>("bawhip"));
        taxiSquadContractColumn.setCellValueFactory(new PropertyValueFactory<PlayerPresentor, String>("contract"));
        taxiSquadSalaryColumn.setCellValueFactory(new PropertyValueFactory<PlayerPresentor, Double>("salary"));
       
        taxiSquadnoteColumn.setCellValueFactory(new PropertyValueFactory<PlayerPresentor, String>("notes"));
       
        taxiSquadTable.setItems(players);
    }
    private void initDraftScreen(){
         draftScreenPane = new VBox();
         draftScreenHeadingLabel = initChildLabel(draftScreenPane,
                WDK_PropertyType.DRAFT_SCREEN_HEADING_LABEL, 
                CLASS_HEADING_LABEL);
         sumaryButtonToolbar = new HBox();
         selectPlayerButton = initChildButton(sumaryButtonToolbar,
                WDK_PropertyType.STAR_ICON, WDK_PropertyType.SELECT_PLAYER_TOOLTIP, false);
        autoDraftButton = initChildButton(sumaryButtonToolbar,
                    WDK_PropertyType.PLAY_ICON, WDK_PropertyType.START_AUTODRAFT_TOOLTIP, false);
        pauseDraftButton = initChildButton(sumaryButtonToolbar,
                    WDK_PropertyType.PAUSE_ICON, WDK_PropertyType.PAUSE_TOOLTIP, false);
        draftScreenPane.getChildren().add(sumaryButtonToolbar);
        initSumaryTable();
        draftScreenPane.getChildren().add(sumaryPlayerTable);
        
    }
    private void initSumaryTable(){
        sumaryPlayerTable = new TableView();
        orderSumaryPlayerColumn = new TableColumn(COL_ORDER);
        sumaryPlayerLastColumn = new TableColumn(COL_LAST);
        //proTeamsLastColumn.setMinWidth(100);
        sumaryPlayerFirstColumn = new TableColumn(COL_FIRST);
        playersOfSummaryTab = FXCollections.observableArrayList();
        //proTeamsFirstColumn.setMinWidth(100);
        sumaryPlayerTeamCol = new TableColumn(COL_FANTASY_TEAM); 
        sumaryPlayerContractColumn = new TableColumn("Contract");
        sumaryPlayerSalaryColumn = new TableColumn("Salary ($)");
        sumaryPlayerTable.getColumns().addAll(orderSumaryPlayerColumn,
                sumaryPlayerFirstColumn,sumaryPlayerLastColumn,
                sumaryPlayerTeamCol,sumaryPlayerContractColumn,sumaryPlayerSalaryColumn);
    }
    private void loadSumaryPlayersTable(ObservableList<PlayerPresentor> players){
        sumaryPlayerLastColumn.setCellValueFactory(new PropertyValueFactory<PlayerPresentor, String>("lastName"));
        sumaryPlayerFirstColumn.setCellValueFactory(new PropertyValueFactory<PlayerPresentor, String>("firstName"));
        sumaryPlayerTeamCol.setCellValueFactory(new PropertyValueFactory<PlayerPresentor, FantasyTeam>("fantasyTeam"));
        orderSumaryPlayerColumn.setCellValueFactory(new PropertyValueFactory<PlayerPresentor, Integer>("order"));
        sumaryPlayerContractColumn.setCellValueFactory(new PropertyValueFactory<PlayerPresentor, String>("contract"));
        sumaryPlayerSalaryColumn.setCellValueFactory(new PropertyValueFactory<PlayerPresentor, Double>("salary"));
        //PlayerPresentor presentor = new PlayerPresentor(dataManager.getDraft());
        sumaryPlayerTable.setItems(players); 
    }  
    public void initAutoDraft(){
        ObservableList<PlayerPresentor> freeAgents = dataManager.getDraft().getPlayers();
        ObservableList<FantasyTeam> teams = dataManager.getDraft().getFantasyTeamsStanding();
       if(!lineupsFull){
            PlayerPresentor player = freeAgents.get(playerIndex);
            if(teamIndex<teams.size()){
                 FantasyTeam team = teams.get(teamIndex);
                 if(team.getPlayerNeeded()>0 ){
                      while(team.getAvailablePosPlayer(player).size()==0){
                          if(playerIndex<freeAgents.size()-1){
                              playerIndex++;
                              player = freeAgents.get(playerIndex);
                          }else
                              playerIndex =0;
                      }
                      player.setContract("S2");
                      player.setAssignedPosition(team.getAvailablePosPlayer(player).get(0));
                      player.setFantasyTeam(team);
                      player.setSalary(1);
                      pickOrder++;
                      player.setOrder(pickOrder);
                      team.addPlayer(player);

                      playersOfSummaryTab.add(player);
                      freeAgents.remove(player);
                 }else if(team.getPlayerNeeded()==0){
                     teamIndex++;
                 }
             }else{
                lineupsFull = true;
                teamIndex = 0;
                playerIndex = 0;
            }
       }else{
                 initAutoDraftTaxiSquad();   
           }
       
       loadSumaryPlayersTable(playersOfSummaryTab);
    }
    public void initAutoDraftTaxiSquad(){
        ObservableList<PlayerPresentor> freeAgents = dataManager.getDraft().getPlayers();
        ObservableList<FantasyTeam> teams = dataManager.getDraft().getFantasyTeamsStanding();
         
    //pickOrder;playerIndex;teamIndex;
       // assume palyers are already ranked
       PlayerPresentor player = freeAgents.get(playerIndex);
       if(teamIndex<teams.size()){
            FantasyTeam team = teams.get(teamIndex);
            if(team.getTaxiPlayerNeeded()>0 ){
                while(team.getTaxiPosition(player).size()==0){
                     if(playerIndex<freeAgents.size()-1){
                         playerIndex++;
                         player = freeAgents.get(playerIndex);
                         
                     }else
                         playerIndex =0;
                 }
                 player.setContract("X");
                 player.setAssignedPosition(team.getTaxiPosition(player).get(0));
                 player.setFantasyTeam(team);
                 player.setSalary(1);
                 pickOrder++;
                 player.setOrder(pickOrder);
                 team.addPlayerToTaxiSquad(player);
                 //team.addPlayer(player);
                 playersOfSummaryTab.add(player);
                 freeAgents.remove(player);
            }else if(team.getTaxiPlayerNeeded()==0){
                teamIndex++;
            }
        }
       loadSumaryPlayersTable(playersOfSummaryTab);
    }
    private void initPalyerScreen(){
        playerScreenBox = new VBox();
        playerScreenHeadingLabel = initChildLabel(playerScreenBox,
                WDK_PropertyType.PLAYER_SCREEN_HEADING_LABEL, 
                CLASS_HEADING_LABEL);
        playerItemsToolbar = new HBox();
        addPlayerButton = initChildButton(playerItemsToolbar,
                WDK_PropertyType.ADD_ICON, WDK_PropertyType.ADD_ITEM_TOOLTIP, false);
        removePlayerButton = initChildButton(playerItemsToolbar,
                WDK_PropertyType.MINUS_ICON, WDK_PropertyType.REMOVE_ITEM_TOOLTIP, false);
        searchBox = new HBox();
        searchLabel = initChildLabel(searchBox,
                WDK_PropertyType.SEARCH_LABEL, 
                CLASS_PROMPT_LABEL);
        searchField = new TextField();
        searchField.setPrefWidth(LARGE_TEXT_FIELD_LENGTH);
        
        searchBox.getChildren().add(searchField);
        //Insets margin = new Insets(50);
        searchBox.setSpacing(5.0);
        searchBox.setTranslateX(45);
        topToolbar = new FlowPane();
        //topToolbar.setStyle("-fx-background-color: gray");
        topToolbar.getChildren().addAll(playerItemsToolbar,searchBox);
        playerScreenBox.getChildren().add(topToolbar);
        initRadioButtons();
        //initTablePalyers();
        playerScreenBox.getChildren().add(radioButtonsBox);
       // playerScreenBox.getChildren().add(playerTable);
        
    }
    
    private void initRadioButtons(){
        allRadioButton = new RadioButton("All");
        catcherRadioButton = new RadioButton("C");
        firstBaseRadioButton = new RadioButton("1B");
        secondBaseRadioButton = new RadioButton("2B");
        thirdBaseRadioButton = new RadioButton("3B");
        cornerInfRadioButton = new RadioButton("CI");
        middleInfRadioButton = new RadioButton("MI");
        shortStopRadioButton = new RadioButton("SS");
        outFieldInfRadioButton = new RadioButton("OF");
        utilityRadioButton = new RadioButton("U");
        pRadioButton = new RadioButton("P");
        group = new ToggleGroup();
        group.getToggles().addAll(allRadioButton,catcherRadioButton, 
                firstBaseRadioButton,secondBaseRadioButton,
                thirdBaseRadioButton,cornerInfRadioButton,
                middleInfRadioButton,shortStopRadioButton,
                outFieldInfRadioButton,utilityRadioButton,pRadioButton);
        radioButtonsBox = new HBox();
        radioButtonsBox.getStyleClass().add(CLASS_BORDERED_PANE);
        radioButtonsBox.setSpacing(20);
        radioButtonsBox.getChildren().addAll(allRadioButton,catcherRadioButton, 
                firstBaseRadioButton,secondBaseRadioButton,
                thirdBaseRadioButton,cornerInfRadioButton,
                middleInfRadioButton,shortStopRadioButton,
                outFieldInfRadioButton,utilityRadioButton,pRadioButton);
        
        
    }
    private void initTablePalyers(){
        playerTable = new TableView();
       // playerTable.getStyleClass().add(CLASS_BORDERED_PANE);
        lastColumn = new TableColumn(COL_LAST);
        lastColumn.setMinWidth(100);
        firstColumn = new TableColumn(COL_FIRST);
        firstColumn.setMinWidth(100);
        teamColumn = new TableColumn(COL_TEAM);
        teamColumn.setMinWidth(100);
        positionColumn = new TableColumn(COL_POSITION);
        positionColumn.setMinWidth(100);
        birthColumn = new TableColumn(COL_BIRTH_YEAR);
        birthColumn.setMinWidth(100);
        r_wColumn = new TableColumn(COL_R_W);
        r_wColumn.setMinWidth(100);
        hr_svColumn = new TableColumn(COL_HR_SV);
        hr_svColumn.setMinWidth(100);
        rbi_kColumn = new TableColumn(COL_RBI_K);
        rbi_kColumn.setMinWidth(100);
        sb_eraColumn = new TableColumn(COL_SB_ERA);
        sb_eraColumn.setMinWidth(100);
        ba_whipColumn = new TableColumn(COL_BA_WHIP);
        ba_whipColumn.setMinWidth(100);
        valueColumn = new TableColumn(COL_VALUE);
        valueColumn.setMinWidth(100);
        noteColumn = new TableColumn(COL_NOTE);
        noteColumn.setMinWidth(100);
       
        noteColumn.setEditable(true);
        playersChange = dataManager.getDraft().getPlayers();
        loadPlayersTable(playersChange);
        playerTable.getColumns().addAll(lastColumn, firstColumn,teamColumn,
               positionColumn, birthColumn, r_wColumn, hr_svColumn, rbi_kColumn,
              sb_eraColumn,ba_whipColumn,valueColumn, noteColumn);
        playerScreenBox.getChildren().add(playerTable);
        
    }
    
    private void loadPlayersTable(ObservableList<PlayerPresentor> players){
        
        lastColumn.setCellValueFactory(new PropertyValueFactory<PlayerPresentor, String>("lastName"));
        firstColumn.setCellValueFactory(new PropertyValueFactory<PlayerPresentor, String>("firstName"));
        teamColumn.setCellValueFactory(new PropertyValueFactory<PlayerPresentor, String>("team"));
        positionColumn.setCellValueFactory(new PropertyValueFactory<PlayerPresentor, String>("position"));
        birthColumn.setCellValueFactory(new PropertyValueFactory<PlayerPresentor, Integer>("birthYear"));
        r_wColumn.setCellValueFactory(new PropertyValueFactory<PlayerPresentor, Integer>("rw"));
        hr_svColumn.setCellValueFactory(new PropertyValueFactory<PlayerPresentor, Integer>("hrsv"));
        rbi_kColumn.setCellValueFactory(new PropertyValueFactory<PlayerPresentor, Integer>("rbik"));
        sb_eraColumn.setCellValueFactory(new PropertyValueFactory<PlayerPresentor, Double>("sbera"));
        ba_whipColumn.setCellValueFactory(new PropertyValueFactory<PlayerPresentor, Double>("bawhip"));
        noteColumn.setCellValueFactory(new PropertyValueFactory<PlayerPresentor, String>("notes"));
        playersChange = players;
       // PlayerPresentor presentor = new PlayerPresentor(dataManager.getDraft());
        playerTable.setItems(playersChange);
        
    }   
    
    //playerRadioButton[i].setToggleGroup(group);
     
    private void initMLBScreen(){
         MLBScreenPane = new VBox();
         MLBScreenHeadingLabel = initChildLabel(MLBScreenPane,
                WDK_PropertyType.MLB_SCREEN_HEADING_LABEL, 
                CLASS_HEADING_LABEL);
         
         proTeamsToolbar = new HBox();
         proTeamsToolbar.setSpacing(10.0);
         proTeamLabel = new Label(PROTEAM_PROMPT);
         proTeamLabel.getStyleClass().add(CLASS_PROMPT_LABEL);
         proTeamsCombox = new ComboBox();
         proTeamsCombox.setItems(dataManager.getDraft().getProTeams());
         proTeamsToolbar.getChildren().addAll(proTeamLabel,proTeamsCombox);
         MLBScreenPane.getChildren().add(proTeamsToolbar);
         initMLBProTeamsPlayerTable();
         MLBScreenPane.getChildren().add(proTeamsPlayerTable);
         MLBScreenPane.setSpacing(10.0);
         
    }
    private void initMLBProTeamsPlayerTable(){
        proTeamsPlayerTable = new TableView();
        proTeamsLastColumn = new TableColumn(COL_LAST);
        //proTeamsLastColumn.setMinWidth(100);
        proTeamsFirstColumn = new TableColumn(COL_FIRST);
        //proTeamsFirstColumn.setMinWidth(100);
        proTeamsPositionsCol = new TableColumn(COL_POSITIONS); 
        proTeamsPlayerTable.getColumns().addAll(proTeamsFirstColumn,
                proTeamsLastColumn,proTeamsPositionsCol);
        
    }
    private void loadProTeamsPlayerTable(ObservableList<PlayerPresentor> players){
        proTeamsLastColumn.setCellValueFactory(new PropertyValueFactory<PlayerPresentor, String>("lastName"));
        proTeamsFirstColumn.setCellValueFactory(new PropertyValueFactory<PlayerPresentor, String>("firstName"));
        //teamColumn.setCellValueFactory(new PropertyValueFactory<PlayerPresentor, String>("team"));
        proTeamsPositionsCol.setCellValueFactory(new PropertyValueFactory<PlayerPresentor, String>("position"));
        proTeamsPlayerTable.setItems(players);
        proTeamsFirstColumn.sortNodeProperty();
    }
    private Button initChildButton(Pane toolbar, WDK_PropertyType icon, WDK_PropertyType tooltip, boolean disabled) {
        PropertiesManager props = PropertiesManager.getPropertiesManager();
        String imagePath = "file:" + PATH_IMAGES + props.getProperty(icon.toString());
        Image buttonImage = new Image(imagePath);
        Button button = new Button();
        button.setDisable(disabled);
        button.setGraphic(new ImageView(buttonImage));
        Tooltip buttonTooltip = new Tooltip(props.getProperty(tooltip.toString()));
        button.setTooltip(buttonTooltip);
        toolbar.getChildren().add(button);
        return button;
    }
    //********
    private Label initChildLabel(Pane container, WDK_PropertyType labelProperty, String styleClass) {
        Label label = initLabel(labelProperty, styleClass);
        container.getChildren().add(label);
        return label;
    }
    private Label initLabel(WDK_PropertyType labelProperty, String styleClass) {
        PropertiesManager props = PropertiesManager.getPropertiesManager();
        String labelText = props.getProperty(labelProperty);
        Label label = new Label(labelText);
        label.getStyleClass().add(styleClass);
        return label;
    }
    //*******
   
    private void initWindow(String windowTitle) {
        // SET THE WINDOW TITLE
        primaryStage.setTitle(windowTitle);

        // GET THE SIZE OF THE SCREEN
        Screen screen = Screen.getPrimary();
        Rectangle2D bounds = screen.getVisualBounds();

        // AND USE IT TO SIZE THE WINDOW
        primaryStage.setX(bounds.getMinX());
        primaryStage.setY(bounds.getMinY());
        primaryStage.setWidth(bounds.getWidth());
        primaryStage.setHeight(bounds.getHeight());

        // ADD THE TOOLBAR ONLY, NOTE THAT THE WORKSPACE
        // HAS BEEN CONSTRUCTED, BUT WON'T BE ADDED UNTIL
        // THE USER STARTS EDITING A COURSE
        wdkPane = new BorderPane();
        wdkPane.setTop(fileToolbarPane);
        //
        String imagePath2 = "file:" + PATH_IMAGES+"Robinson-Cano.gif";
        Image screenImage2  = new Image(imagePath2);
        ImageView view2 = new ImageView(screenImage2);
        String imagePath = "file:" + PATH_IMAGES+"HomeScreen.gif";
        Image screenImage  = new Image(imagePath);
        ImageView view = new ImageView(screenImage);
        HBox box = new HBox();
        box.getChildren().addAll(view2, view);
        
        wdkPane.setCenter(view2);
        wdkPane.getStyleClass().add(CLASS_SUBJECT_PANE);
        //wdkPane.getCenter().getStyleClass().add(CLASS_SUBJECT_PANE);
        primaryScene = new Scene(wdkPane);

        // NOW TIE THE SCENE TO THE WINDOW, SELECT THE STYLESHEET
        // WE'LL USE TO STYLIZE OUR GUI CONTROLS, AND OPEN THE WINDOW
        //@toDo
        primaryScene.getStylesheets().add(PRIMARY_STYLE_SHEET);
      
        primaryStage.setScene(primaryScene);
        primaryStage.show();
    }
    private void initEventHandlers(){
       
         fileController = new FileController(messageDialog, yesNoCancelDialog, draftFileManager, siteExporter);
         newDraftButton.setOnAction(e->{
            fileController.handleNewDraftRequest(this);
            //Draft draft = dataManager.getDraft();
            //loadPlayersTable(playersChange);
            initTablePalyers();
           // playerScreenBox.getChildren().add(playerTable);
        
             System.out.println("new Draft");
             initPlayerTableEventHandler();
             initFantasyTeamComboBoxHandler();
             initLineUpTableEventHandler();
             initProTeamComboBoxHandler();
            
         });
         /*loadDraftButton.setOnAction(e->{
             fileController.handleLoadDraftRequest(this, dataManager.getDraft());
         });*/
         saveDraftButton.setOnAction(e->{
             fileController.handleSaveDraftRequest(this, dataManager.getDraft());
         });
         loadDraftButton.setOnAction(e->{
             draftEditController.enable(true);
             fileController.handleLoadDraftRequest(this);
             initTablePalyers();
             initPlayerTableEventHandler();
             initFantasyTeamComboBoxHandler();
             initLineUpTableEventHandler();
             initProTeamComboBoxHandler();
         });
         exitButton.setOnAction(e->{
             //System.exit(0);
             fileController.handleExitRequest(this);
         });
         homeScreenButton.setOnAction(e->{
             fileController.handleSwitchScreen(homeScreenPane, workspacePane);
         });
          draftScreenButton.setOnAction(e->{
             fileController.handleSwitchScreen(draftScreenPane, workspacePane);
         });
         standingScreenButton.setOnAction(e->{
             fileController.handleSwitchScreen(fantasyTeamPane, workspacePane);
              loadFantasyStandingTable(dataManager.getDraft().getFantasyTeamsStanding());
              dataManager.getDraft().calculatePoints();
         });
        MLBScreenButton.setOnAction(e->{
           fileController.handleSwitchScreen(MLBScreenPane, workspacePane);
       });
       playerScreenButton.setOnAction(e->{
          fileController.handleSwitchScreen(playerScreenBox, workspacePane);
       });
       registerTextFieldController(searchField);
       allRadioButton.setOnAction(e->{
           loadPlayersTable(dataManager.getDraft().getPosition("All", playersChange));
       });
       firstBaseRadioButton.setOnAction(e->{
           loadPlayersTable(dataManager.getDraft().getPosition("1B", playersChange));
       });
       secondBaseRadioButton.setOnAction(e->{
           loadPlayersTable(dataManager.getDraft().getPosition("2B", playersChange));
       });
       cornerInfRadioButton.setOnAction(e->{
           loadPlayersTable(dataManager.getDraft().getPosition("CI", playersChange));
       });
       middleInfRadioButton.setOnAction(e->{
           loadPlayersTable(dataManager.getDraft().getPosition("MI", playersChange));
       });
      shortStopRadioButton.setOnAction(e->{
           loadPlayersTable(dataManager.getDraft().getPosition("SS", playersChange));
        });
       outFieldInfRadioButton.setOnAction(e->{
           loadPlayersTable(dataManager.getDraft().getPosition("OF", playersChange));
       });
       utilityRadioButton.setOnAction(e->{
           loadPlayersTable(dataManager.getDraft().getPosition("U", playersChange));
       });
       pRadioButton.setOnAction(e->{
           loadPlayersTable(dataManager.getDraft().getPosition("P", playersChange));
       });
       thirdBaseRadioButton.setOnAction(e->{
           loadPlayersTable(dataManager.getDraft().getPosition("3B", playersChange));
       });
       catcherRadioButton.setOnAction(e->{
           loadPlayersTable(dataManager.getDraft().getPosition("C", playersChange));
       });
       draftEditController = new DraftEditController(primaryStage, dataManager.getDraft(),
              messageDialog, yesNoCancelDialog);
       addTeamButton.setOnAction(e->{
           draftEditController.handleAddFantasyTeam(this);
       });
       removeTeamButton.setOnAction(e->{
           FantasyTeam team = (FantasyTeam) fantasyTeamComboBox.getSelectionModel().getSelectedItem();
           draftEditController.handleRemoveFantasyTeam(this, team);
           fantasyTeamComboBox.setItems(dataManager.getDraft().getFantasyTeams());
       });
       editTeamButton.setOnAction(e->{
           FantasyTeam team = (FantasyTeam) fantasyTeamComboBox.getSelectionModel().getSelectedItem();
           draftEditController.handleEditFantasyTeam(this, team);
           fantasyTeamComboBox.setItems(dataManager.getDraft().getFantasyTeams());
       });
       addPlayerButton.setOnAction(e->{ 
           ObservableList<String> teams = dataManager.getDraft().getProTeams();
           draftEditController.handleAddPlayer(this, teams);
       });
       removePlayerButton.setOnAction(e->{
           PlayerPresentor player = (PlayerPresentor) playerTable.getSelectionModel().getSelectedItem();
           draftEditController.handleRemovePlayerItem(this, player);
           playerTable.setItems(dataManager.getDraft().getPlayers());
       });
       selectPlayerButton.setOnAction(e->{
           draftEditController.handleSelectPlayerRequest(this);
           
       });
       autoDraftButton.setOnAction(e->{
           isPause = false;
           if(!isThreadRunning){
             try {
                 start();
                 isThreadRunning = true;
             } catch (Exception ex) {
                 Logger.getLogger(WDK_UI.class.getName()).log(Level.SEVERE, null, ex);
             }
           }
       });
       pauseDraftButton.setOnAction(e->{
           isPause = true;
       });
    }

    public void updateToolbarControls(boolean saved) {
        saveDraftButton.setDisable(saved);

        // ALL THE OTHER BUTTONS ARE ALWAYS ENABLED
        // ONCE EDITING THAT FIRST COURSE BEGINS
        loadDraftButton.setDisable(false);
        exportSiteButton.setDisable(false);
    }

    public DraftDataManager getDataManager() {
        return dataManager; 
    }

    
    public void setDataManager(DraftDataManager initDataManager) {
        dataManager = initDataManager;
    }

    @Override
    public void reloadDraft(Draft draftToReload) {
        if(!workspaceActivated)
            activateWorkspace();
        
        //draftEditController.enable(false);
        if(draftEditController.isEnable()){
            draftNameField.setText(draftToReload.getDraftName());
            
            loadPlayersTable(draftToReload.getPlayers());
            //System.out.println("Player 1 "+draftToReload.getPlayers().get(0));
           // fantasyTeamComboBox.setItems(draftToReload.getFantasyTeams());   
           // System.out.print("UI LOADED ? in UI reload()");
        //System.out.println(" fteam Pal "+draftToReload.getFantasyTeams().get(1).getPlayers().get(0));
        }
        draftEditController.enable(false);
        
    }
    public void activateWorkspace() {
        if (!workspaceActivated) {
            // PUT THE WORKSPACE IN THE GUI
            //wdkPane.getStyleClass().add(CLASS_SUBJECT_PANE);
            //
            wdkPane.setCenter(workspaceScrollPane);
            wdkPane.setBottom(screenSelectionToolbarPane);
           
            workspaceActivated = true;
        }
    }
   private void initPlayerTableEventHandler(){
       playerTable.setOnMouseClicked(e -> {
            if (e.getClickCount() == 2) {
                // OPEN UP THE SCHEDULE ITEM EDITOR
                
                PlayerPresentor pl = (PlayerPresentor) playerTable.getSelectionModel().getSelectedItem();
                PlayerEditController playerEditController = new PlayerEditController(primaryStage, pl,
                messageDialog, yesNoCancelDialog);
                ObservableList<FantasyTeam> teams = dataManager.getDraft().getFantasyTeams();
                playerEditController.handleEditPlayer(this, pl, teams);
                //loadFantasyStandingTable(dataManager.getDraft().getFantasyTeamsStanding());
            }
   });
   }
   private void initLineUpTableEventHandler(){
       lineUpTable.setOnMouseClicked(e -> {
            if (e.getClickCount() == 2) {
                // OPEN UP THE SCHEDULE ITEM EDITOR
                PlayerPresentor pl = (PlayerPresentor) lineUpTable.getSelectionModel().getSelectedItem();
                PlayerEditController playerEditController = new PlayerEditController(primaryStage, pl,
                messageDialog, yesNoCancelDialog);
                ObservableList<FantasyTeam> teams = dataManager.getDraft().getFantasyTeams();
                playerEditController.handleEditPlayer(this, pl, teams);
                //loadFantasyStandingTable(dataManager.getDraft().getFantasyTeamsStanding());
            }
   });
   }
   private void initFantasyTeamComboBoxHandler(){
       fantasyTeamComboBox.setOnAction(e->{
           FantasyTeam tea = (FantasyTeam) fantasyTeamComboBox.getSelectionModel().getSelectedItem();
           loadLineUpTable(tea.getPlayers());
           loadTaxiSquadTable(tea.getTaxiSquad());
           
       });
   }
   
   private void initProTeamComboBoxHandler(){
       proTeamsCombox.setOnAction(e->{
           String tea =  proTeamsCombox.getSelectionModel().getSelectedItem().toString();
           loadProTeamsPlayerTable(dataManager.getDraft().getProTeamsPlayers(tea));
       });
   }
   public void setDraftFileManager(DraftFileManager draftFile){
       draftFileManager = draftFile;
   }
   private void registerTextFieldController(TextField textField) {
        textField.textProperty().addListener((observable, oldValue, newValue) -> {
            loadPlayersTable(dataManager.getDraft().getSetOfPlayers(newValue));
        });
    }

    public Stage getWindow() {
        return primaryStage;
    }
    
}
