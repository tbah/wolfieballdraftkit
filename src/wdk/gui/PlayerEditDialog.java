/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package wdk.gui;

import java.util.ArrayList;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.geometry.Insets;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.ComboBox;
import javafx.scene.control.Label;
import javafx.scene.control.RadioButton;
import javafx.scene.control.TextField;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.HBox;
import javafx.scene.layout.VBox;
import javafx.stage.Modality;
import javafx.stage.Stage;
import javax.swing.JOptionPane;
import wdk.data.Draft;
import wdk.data.FantasyTeam;
import wdk.data.PlayerPresentor;
import static wdk.gui.WDK_UI.CLASS_HEADING_LABEL;
import static wdk.gui.WDK_UI.CLASS_PROMPT_LABEL;
import static wdk.gui.WDK_UI.PRIMARY_STYLE_SHEET;

/**
 *
 * @author Thierno
 */
public class PlayerEditDialog extends Stage {
    
PlayerPresentor player;
    
    // GUI CONTROLS FOR OUR DIALOG
    GridPane gridPane;
    Scene dialogScene;
    Label headingLabel;
    Label firstNameLabel;
    TextField firstNameTextField;
    Label lastNameLabel;
    TextField lastNameTextField;
    Button completeButton;
    Button cancelButton;
    
    // THIS IS FOR KEEPING TRACK OF WHICH BUTTON THE USER PRESSED
    String selection;
    public static final String FLAGS_IMAGES = "./images/wolfieball_images/flags/";
    public static final String PLAYERS_IMAGES = "./images/wolfieball_images/players/";
    public static final String PLAYERS_IMAGES_MISSING = "./images/wolfieball_images/players/AAA_PhotoMissing.jpg";
    public static final String COMPLETE = "Complete";
    public static final String CANCEL = "Cancel";
    public static final String FANTASY_TEAM_PROMPT = "Fntasy Team: ";
    public static final String POSITION_PROMPT = "Position: ";
    public static final String CONTRACT_PROMPT = "Contract: ";
    public static final String SALARY_PROMPT = "Salary($): ";
    public static final String PLAYER_ITEM_HEADING_LABEL = "Player Details";
    public static final String ADD_PLAYER_ITEM_TITLE = "Add New Player";
    public static final String EDIT_PLAYER_ITEM_TITLE = "Edit Player";
   
   
    private Label fantasyTeamLabel;
    private ComboBox<FantasyTeam> fantasyTeamComboBox;
    private Label positionLabel;
    private ComboBox<String> positionComboBox;
    private Label contractLabel;
    private ComboBox<String> contractComboBox;
    private Label salaryLabel;
    private TextField salaryTextField;
    private ImageView playerPhotoView;
    private ImageView playerFlagView;
    private Label nameLabel;
    private Label positionsLabel;
    Stage primaryStage;
    Draft draft;
    MessageDialog messageDialog;
    
    public PlayerEditDialog(Stage primaryStage, PlayerPresentor draft,  MessageDialog messageDialog) {       
        // MAKE THIS DIALOG MODAL, MEANING OTHERS WILL WAIT
        // FOR IT WHEN IT IS DISPLAYED
        initModality(Modality.WINDOW_MODAL);
        initOwner(primaryStage);
        player = draft;
        // FIRST OUR CONTAINER
        gridPane = new GridPane();
        gridPane.setPadding(new Insets(10, 20, 20, 20));
        gridPane.setHgap(10);
        gridPane.setVgap(10);
        
        // PUT THE HEADING IN THE GRID, NOTE THAT THE TEXT WILL DEPEND
        // ON WHETHER WE'RE ADDING OR EDITING
        headingLabel = new Label(PLAYER_ITEM_HEADING_LABEL);
        headingLabel.getStyleClass().add(CLASS_HEADING_LABEL);
        String imageDescription = player.getLastName()+player.getFirstName();
        String flagDescription = player.getNationality();
        Image playerImage = new Image("file:"+PLAYERS_IMAGES+imageDescription+".jpg");
        
        if(playerImage.isError()){
            playerImage = new Image("file:"+PLAYERS_IMAGES_MISSING);
        }
        playerPhotoView = new ImageView(playerImage);
        Image playerFlag = new Image("file:"+FLAGS_IMAGES+flagDescription+".png");
         if(playerFlag.isError()){
            playerImage = new Image("file:"+FLAGS_IMAGES+"USA.png");
        }
        playerFlagView = new ImageView(playerFlag);
        nameLabel = new Label(player.getFirstName()+" "+player.getLastName());
        // NOW THE DESCRIPTION 
        nameLabel.getStyleClass().add(CLASS_PROMPT_LABEL);
        positionLabel = new Label(player.getPosition());
        positionLabel.getStyleClass().add(CLASS_PROMPT_LABEL);
        fantasyTeamLabel = new Label(FANTASY_TEAM_PROMPT);
        fantasyTeamComboBox = new ComboBox();
        fantasyTeamLabel.getStyleClass().add(CLASS_PROMPT_LABEL);
        VBox topRightBox = new VBox();
        topRightBox.getChildren().addAll(playerFlagView, nameLabel,positionLabel);
        topRightBox.setSpacing(10);
       
        
        positionsLabel = new Label(POSITION_PROMPT);
        positionsLabel.getStyleClass().add(CLASS_PROMPT_LABEL);
        
        positionComboBox = new ComboBox();
        
        
        contractLabel = new Label(CONTRACT_PROMPT);
        contractLabel.getStyleClass().add(CLASS_PROMPT_LABEL);
        contractComboBox = new ComboBox();
        String [] contract = {"S1","S2","X"};
        for(int i = 0; i<contract.length; i++){
            contractComboBox.getItems().add(contract[i]);
        }
        
        salaryLabel = new Label(SALARY_PROMPT);
        salaryLabel.getStyleClass().add(CLASS_PROMPT_LABEL);
        salaryTextField = new TextField();
        salaryTextField.textProperty().addListener((observable, oldValue, newValue) -> {
            if(isAllowed(newValue)){
                if(newValue.length()!=0)
              player.setSalary(newValue);
                else
                    player.setSalary("1");
            }
            else{
                JOptionPane.showMessageDialog(null, "Invalid input"+
                        "\nRetype integer or decimal number." );
                salaryTextField.setText(oldValue);
            }
        });
        
        
        
        // AND FINALLY, THE BUTTONS
        completeButton = new Button(COMPLETE);
        cancelButton = new Button(CANCEL);
        
        // REGISTER EVENT HANDLERS FOR OUR BUTTONS
        EventHandler completeCancelHandler = (EventHandler<ActionEvent>) (ActionEvent ae) -> {
            Button sourceButton = (Button)ae.getSource();
            PlayerEditDialog.this.selection = sourceButton.getText();
            PlayerEditDialog.this.hide();
        };
        completeButton.setOnAction(completeCancelHandler);
        cancelButton.setOnAction(completeCancelHandler);

        // NOW LET'S ARRANGE THEM ALL AT ONCE
        gridPane.add(headingLabel, 0, 0, 2, 1);
        gridPane.add(playerPhotoView, 0, 1, 1, 1);
        gridPane.add(topRightBox, 1, 1, 1, 1);
        gridPane.add(fantasyTeamLabel, 0, 2, 1, 1);
        gridPane.add(fantasyTeamComboBox, 1, 2, 1, 1);
        gridPane.add(positionsLabel, 0, 4, 1, 1);
        gridPane.add(positionComboBox, 1, 4, 1, 1);
        gridPane.add(contractLabel, 0, 5, 1, 1);
        gridPane.add(contractComboBox, 1, 5, 1, 1);
        gridPane.add(salaryLabel, 0, 6, 1, 1);
        gridPane.add(salaryTextField, 1, 6, 1, 1);
        gridPane.add(completeButton, 0, 7, 1, 1);
        gridPane.add(cancelButton, 1, 7, 1, 1);

        // AND PUT THE GRID PANE IN THE WINDOW
        dialogScene = new Scene(gridPane);
        dialogScene.getStylesheets().add(PRIMARY_STYLE_SHEET);
        this.setScene(dialogScene);
       
        
    }

    private boolean isAllowed(String str){
        for(int i = 0; i<str.length(); i++){
            if((int)str.charAt(i)!=46 &&((int)str.charAt(i)<48 || (int)str.charAt(i)>57))
                   return false;
        }
        return true;
            
    }
    private void makeDialog(PlayerPresentor pl){
        
    }
    
    /**
     * Accessor method for getting the selection the user made.
     * 
     * @return Either YES, NO, or CANCEL, depending on which
     * button the user selected when this dialog was presented.
     */
    public String getSelection() {
        return selection;
    }
    
   public PlayerPresentor getPlayerItem() { 
        return player;
    }
    public PlayerPresentor showAddPlayerItemDialog(ObservableList<String> teams) {
        // SET THE DIALOG TITLE
        setTitle(EDIT_PLAYER_ITEM_TITLE);
        
        // RESET THE SCHEDULE ITEM OBJECT WITH DEFAULT VALUES
        player = new PlayerPresentor();
      
        // LOAD THE UI STUFF
        /*topicTextField.setText(lecture.getTopic());
        numberOfLectureComboBox.setValue(lecture.getSessions());*/
        //datePicker.setValue(initDate);
        //topicsTextField.setText(assignment.getTopics());
        
        // AND OPEN IT UP
       this.showAndWait();
        
        return player;
    }
    public void loadGUIData() {
        // LOAD THE UI STUFF
        //lastNameTextField.setText(player.getFirstName());
        //firstNameTextField.setText(player.getLastName());
        //datePicker.setValue(assignment.getDate());
        //topicsTextField.setText(assignment.getTopics());       
    }
    
    public boolean wasCompleteSelected() {
        return selection.equals(COMPLETE);
    }
    public void showEditPlayerDialog(PlayerPresentor itemToEdit, 
            ObservableList<FantasyTeam> fantasyTeams) {
        // SET THE DIALOG TITLE
        FantasyTeam freeAgent = fantasyTeams.get(0);
        setTitle(EDIT_PLAYER_ITEM_TITLE);
        
        // LOAD THE SCHEDULE ITEM INTO OUR LOCAL OBJECT
        player = new PlayerPresentor();
        player = itemToEdit;
        fantasyTeamComboBox.setItems(fantasyTeams);
        fantasyTeamComboBox.setOnAction(e->{
            FantasyTeam team = fantasyTeamComboBox.getSelectionModel().getSelectedItem();
            if(team.getName().equals(freeAgent.getName())){
                disableEdit();
                player.setFantasyTeam(team);
            }
            else{
                enableEdit();
                positionComboBox.setItems(team.getAvailablePosPlayer(player));
                player.setFantasyTeam(team);
            }
        });
        positionComboBox.setOnAction(e->{
            player.setAssignedPosition(positionComboBox.getSelectionModel().getSelectedItem());
        });
        contractComboBox.setOnAction(e->{
            player.setContract(contractComboBox.getSelectionModel().getSelectedItem());
        });
        
        makeDialog(itemToEdit);
        // AND THEN INTO OUR GUI
        loadGUIData();
               
        // AND OPEN IT UP
        this.showAndWait();
    }
    private void disableEdit(){
        positionComboBox.setDisable(true);
        contractComboBox.setDisable(true);
        salaryTextField.setDisable(true);
    }
    private void enableEdit(){
        positionComboBox.setDisable(false);
        contractComboBox.setDisable(false);
        salaryTextField.setDisable(false);
    }
    /*public Image loadImage(String imageName) {
        Image img = new Image(ImgPath + imageName);
        return img;
    }*/
   
    
}
