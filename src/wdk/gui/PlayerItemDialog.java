/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package wdk.gui;

import java.util.ArrayList;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.geometry.Insets;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.ComboBox;
import javafx.scene.control.Label;
import javafx.scene.control.RadioButton;
import javafx.scene.control.TextField;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.HBox;
import javafx.stage.Modality;
import javafx.stage.Stage;
import wdk.data.Draft;
import wdk.data.PlayerPresentor;
import static wdk.gui.WDK_UI.CLASS_HEADING_LABEL;
import static wdk.gui.WDK_UI.CLASS_PROMPT_LABEL;
import static wdk.gui.WDK_UI.PRIMARY_STYLE_SHEET;

/**
 *
 * @author Thierno
 */
public class PlayerItemDialog extends Stage {
    
PlayerPresentor player;
    
    // GUI CONTROLS FOR OUR DIALOG
    GridPane gridPane;
    Scene dialogScene;
    Label headingLabel;
    Label firstNameLabel;
    TextField firstNameTextField;
    Label lastNameLabel;
    TextField lastNameTextField;
    Button completeButton;
    Button cancelButton;
    
    // THIS IS FOR KEEPING TRACK OF WHICH BUTTON THE USER PRESSED
    String selection;
    
    public static final String COMPLETE = "Complete";
    public static final String CANCEL = "Cancel";
    public static final String PROTEAM_PROMPT = "Pro Team: ";
    public static final String OWNER_PROMPT = "First Name: ";
    public static final String NAME_PROMPT = "Last Name:";
    public static final String PLAYER_ITEM_HEADING_LABEL = "Player Details";
    public static final String ADD_PLAYER_ITEM_TITLE = "Add New Player";
    public static final String EDIT_TEAM_ITEM_TITLE = "Edit Player";
    private final ComboBox <String> proTeam;
    private final Label proTeamLabel;
    private final RadioButton firstBaseRadioButton;
    private final RadioButton secondBaseRadioButton;
    private final RadioButton thirdBaseRadioButton;
    private final RadioButton shortStopRadioButton;
    private final RadioButton outFieldInfRadioButton;
    private final RadioButton pRadioButton;
    private final HBox radioButtonsBox;
    private final RadioButton catcherRadioButton;
    
    public PlayerItemDialog(Stage primaryStage, Draft draft,  MessageDialog messageDialog) {       
        // MAKE THIS DIALOG MODAL, MEANING OTHERS WILL WAIT
        // FOR IT WHEN IT IS DISPLAYED
        initModality(Modality.WINDOW_MODAL);
        initOwner(primaryStage);
        
        // FIRST OUR CONTAINER
        gridPane = new GridPane();
        gridPane.setPadding(new Insets(10, 20, 20, 20));
        gridPane.setHgap(10);
        gridPane.setVgap(10);
        
        // PUT THE HEADING IN THE GRID, NOTE THAT THE TEXT WILL DEPEND
        // ON WHETHER WE'RE ADDING OR EDITING
        headingLabel = new Label(PLAYER_ITEM_HEADING_LABEL);
        headingLabel.getStyleClass().add(CLASS_HEADING_LABEL);
    
        // NOW THE DESCRIPTION 
        lastNameLabel = new Label(NAME_PROMPT);
        lastNameLabel.getStyleClass().add(CLASS_PROMPT_LABEL);
        lastNameTextField = new TextField();
        lastNameTextField.textProperty().addListener((observable, oldValue, newValue) -> {
            player.setLastName(newValue);
        });
        
        
        firstNameLabel = new Label(OWNER_PROMPT);
        firstNameLabel.getStyleClass().add(CLASS_PROMPT_LABEL);
        
        firstNameTextField = new TextField();
        firstNameTextField.textProperty().addListener((observable, oldValue, newValue) -> {
            player.setFirstName(newValue);
        });
        
        proTeamLabel = new Label(PROTEAM_PROMPT);
        proTeamLabel.getStyleClass().add(CLASS_PROMPT_LABEL);
        proTeam = new ComboBox();
       // ObservableList<String> teams = draft.getProTeams();
        proTeam.setOnAction(e->{
            player.setTeam(proTeam.getSelectionModel().getSelectedItem());
        });
        
        catcherRadioButton = new RadioButton("C");
        catcherRadioButton.setOnAction(e->{ 
            player.addPosition("C");
        });
        firstBaseRadioButton = new RadioButton("1B");
        firstBaseRadioButton.setOnAction(e->{ 
            player.addPosition("1B");
        });
        secondBaseRadioButton = new RadioButton("2B");
        secondBaseRadioButton.setOnAction(e->{ 
            player.addPosition("2B");
        });
        thirdBaseRadioButton = new RadioButton("3B");
        thirdBaseRadioButton.setOnAction(e->{ 
            player.addPosition("3B");
        });
        shortStopRadioButton = new RadioButton("SS");
        shortStopRadioButton.setOnAction(e->{ 
            player.addPosition("SS");
        });
        outFieldInfRadioButton = new RadioButton("OF");
        outFieldInfRadioButton.setOnAction(e->{ 
            player.addPosition("OF");
        });
        pRadioButton = new RadioButton("P");
        pRadioButton.setOnAction(e->{ 
            player.addPosition("P");
        });
        HBox radioBtBox = new HBox();
        radioBtBox.getChildren().addAll(catcherRadioButton, 
                firstBaseRadioButton);
        radioBtBox.setSpacing(10);
        radioButtonsBox = new HBox();
        radioButtonsBox.getChildren().addAll(secondBaseRadioButton,
                thirdBaseRadioButton,shortStopRadioButton,
                outFieldInfRadioButton,pRadioButton);
        radioButtonsBox.setSpacing(10);
        
        // AND FINALLY, THE BUTTONS
        completeButton = new Button(COMPLETE);
        cancelButton = new Button(CANCEL);
        
        // REGISTER EVENT HANDLERS FOR OUR BUTTONS
        EventHandler completeCancelHandler = (EventHandler<ActionEvent>) (ActionEvent ae) -> {
            Button sourceButton = (Button)ae.getSource();
            PlayerItemDialog.this.selection = sourceButton.getText();
            PlayerItemDialog.this.hide();
        };
        completeButton.setOnAction(completeCancelHandler);
        cancelButton.setOnAction(completeCancelHandler);

        // NOW LET'S ARRANGE THEM ALL AT ONCE
        gridPane.add(headingLabel, 0, 0, 2, 1);
        gridPane.add(firstNameLabel, 0, 1, 1, 1);
        gridPane.add(firstNameTextField, 1, 1, 1, 1);
        gridPane.add(lastNameLabel, 0, 2, 1, 1);
        gridPane.add(lastNameTextField, 1, 2, 1, 1);
        gridPane.add(proTeamLabel, 0, 3, 1, 1);
        gridPane.add(proTeam, 1, 3, 1, 1);
        gridPane.add(radioBtBox, 0, 4, 1, 1);
        gridPane.add(radioButtonsBox, 1, 4, 1, 1);
        gridPane.add(completeButton, 0, 5, 1, 1);
        gridPane.add(cancelButton, 1, 5, 1, 1);

        // AND PUT THE GRID PANE IN THE WINDOW
        dialogScene = new Scene(gridPane);
        dialogScene.getStylesheets().add(PRIMARY_STYLE_SHEET);
        this.setScene(dialogScene);
    }
    
    /**
     * Accessor method for getting the selection the user made.
     * 
     * @return Either YES, NO, or CANCEL, depending on which
     * button the user selected when this dialog was presented.
     */
    public String getSelection() {
        return selection;
    }
    
   public PlayerPresentor getPlayerItem() { 
        return player;
    }
    public PlayerPresentor showAddPlayerItemDialog(ObservableList<String> teams) {
        // SET THE DIALOG TITLE
        setTitle(ADD_PLAYER_ITEM_TITLE);
        
        // RESET THE SCHEDULE ITEM OBJECT WITH DEFAULT VALUES
        player = new PlayerPresentor();
        proTeam.setItems(teams);
        // LOAD THE UI STUFF
        /*topicTextField.setText(lecture.getTopic());
        numberOfLectureComboBox.setValue(lecture.getSessions());*/
        //datePicker.setValue(initDate);
        //topicsTextField.setText(assignment.getTopics());
        
        // AND OPEN IT UP
       this.showAndWait();
        
        return player;
    }
    public void loadGUIData() {
        // LOAD THE UI STUFF
        lastNameTextField.setText(player.getFirstName());
        firstNameTextField.setText(player.getLastName());
        //datePicker.setValue(assignment.getDate());
        //topicsTextField.setText(assignment.getTopics());       
    }
    
    public boolean wasCompleteSelected() {
        return selection.equals(COMPLETE);
    }
    /*public void showEditFantasyTeamDialog(FantasyTeam itemToEdit) {
        // SET THE DIALOG TITLE
        setTitle(EDIT_TEAM_ITEM_TITLE);
        
        // LOAD THE SCHEDULE ITEM INTO OUR LOCAL OBJECT
        team = new FantasyTeam(itemToEdit.getName(), itemToEdit.getOwner());
       // lecture.setTopic(itemToEdit.getTopic());
       // lecture.setSessions(itemToEdit.getSessions());
       // lecture.setTopics(itemToEdit.getTopics());
        
        // AND THEN INTO OUR GUI
        loadGUIData();
               
        // AND OPEN IT UP
        this.showAndWait();
    }*/

  
    
}
