/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package wdk.file;

import java.io.IOException;
import java.util.ArrayList;
import wdk.data.Draft;
import wdk.data.Hitter;
import wdk.data.Pitcher;

/**
 *
 * @author Thierno
 */
public interface DraftFileManager {

    public void saveDraft(Draft draftToSave)throws IOException;
    public void loadDraft(Draft draftToLoad,String filePath )throws IOException;
    public ArrayList<Pitcher> loadPitchers(Draft draftToLoad, String filePath) throws IOException;
    public ArrayList<Hitter> loadHitters(Draft draftToLoad, String filePath) throws IOException;
}
