/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package wdk.file;

import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;
import javafx.collections.ObservableList;
import wdk.data.Draft;

import javax.json.Json;
import javax.json.JsonArray;
import javax.json.JsonArrayBuilder;
import javax.json.JsonObject;
import javax.json.JsonReader;
import javax.json.JsonWriter;
import javax.json.JsonValue;
import javax.swing.JOptionPane;
import static javax.swing.JOptionPane.YES_NO_OPTION;
import static wdk.WDK_StartupConstants.PATH_DRAFT;
import wdk.data.FantasyTeam;
import wdk.data.Hitter;
import wdk.data.Pitcher;
import wdk.data.Player;
import wdk.data.PlayerPresentor;

/**
 *
 * @author Thierno
 */
public class JsonFileManager implements DraftFileManager {
    String JSON_HITTERS = "Hitters";
    String JSON_PITCHERS = "Pitchers";
    String JSON_P = "P";
    String JSON_TEAM = "TEAM";
    String PRO_TEAMS = "PRO_TEAMS";
    String JSON_LAST_NAME = "LAST_NAME";
    String JSON_FIRST_NAME = "FIRST_NAME";
    String JSON_QP = "QP";
    String JSON_AB = "AB";
    String JSON_R = "R";
    String JSON_H = "H";
    String JSON_HR = "HR";
    String JSON_RBI = "RBI";
    String JSON_SB = "SB";
    String JSON_NOTES = "NOTES";
    String JSON_YEAR_OF_BIRTH = "YEAR_OF_BIRTH";
    String JSON_NATION_OF_BIRTH = "NATION_OF_BIRTH";
    String JSON_IP = "IP";
    String JSON_ER = "ER";
    String JSON_W = "W";
    String JSON_SV = "SV";
    String JSON_BB = "BB";
    String JSON_K = "K";
    String JSON_ERA = "ERA";
    String JSON_WHIP = "WHIP";
    String JSON_BA = "BA";
    String JSON_PP = "PP";
    String JSON_PLAYER_NEED = "PL_NEED";
    String JSON_BUDJET = "BUDJET";
    String JSON_POINTS = "POINTS";
    String JSON_PLAYER = "PLAYERS";
    String JSON_TEAMS = "TEAMS";
    
    String JSON_EXT = ".json";
    String SLASH = "/";
    String DRAFT_NAME = "DRAFT_NAME";
    String PLAYER_FIRSTNAME= "FIRST_NAME";
    String PLAYER_LASTNAME = "LAST_NAME";
    String PLAYER_NATION = "NATION";
    String PLAYER_BIRTH = "BIRTH_YEAR";
    String PLAYER_BAWHIP = "BAWHIP";
    String PLAYER_CONTRACT = "CONTRACT";
    String PLAYER_SBERA = "SBERA";
    String PLAYER_HR = "HR";
    String PLAYER_HRSV = "HRSV";
    String PLAYER_PRO_TEAM ="PLAYER_PRO_TEAM";
    String PLAYER_NOTES = "NOTES";
    String PLAYER_POSITIONS = "POSITIONS";
    String PLAYER_RBIK = "RBIK";
    String PLAYER_RW = "RW";
    String PLAYER_ASSIG_POS = "ASSIG_POS";
    String TEAM_NAME = "NAME";
    String FANTASY_TEAM = "FANTASY_TEAM";
    String TEAM_OWNER = "OWNER";
    String TEAM_START_LINE = "START_LINE";
    String TEAM_TAXI_SQUAD = "TAXI_SQUAD";

    
    
    ArrayList<String> teams; 
    ArrayList<Pitcher> pitchers;
    ArrayList<Hitter> hitters;
    public JsonFileManager(){
        teams = new ArrayList();
        pitchers = new ArrayList();
        hitters = new ArrayList();
    }

    /**
     *
     * @param draftToSave
     * @throws IOException
     */
    @Override
    public void saveDraft(Draft draftToSave)throws IOException {
       String draftListing = ""+draftToSave.getDraftName();
       //System.out.println("Draft name "+draftListing+" size "+draftListing.length());
       if(draftToSave.getDraftName()== null){
           
           draftListing = JOptionPane.showInputDialog(null, "Please enter the Draft name","Draft Name");
           draftToSave.setDraftName(draftListing);
       }
       String jsonFilePath = PATH_DRAFT+SLASH+draftListing+JSON_EXT;
       OutputStream os = new FileOutputStream(jsonFilePath);
       JsonWriter jsonWriter = Json.createWriter(os);
       
       JsonArray playersJsonArray = makePLayersJsonArray(draftToSave.getPlayers());
       JsonArray fantasyTeamsJsonArray = makeFantasyTeamJsonArray(draftToSave.getFantasyTeams());
       JsonArray proTeamsJsonArray = makeProTeamJsonArray(draftToSave.getProTeams());
       JsonObject draftJsonObject = Json.createObjectBuilder()
               .add(DRAFT_NAME, draftToSave.getDraftName())
               .add(JSON_PLAYER, playersJsonArray)
               .add(JSON_TEAMS,fantasyTeamsJsonArray)
               .add(PRO_TEAMS, proTeamsJsonArray).build();
               
       
       jsonWriter.writeObject(draftJsonObject);         
    }
    @Override
    public void loadDraft(Draft draftToLoad, String jsonFilePath)throws IOException {
        JsonObject json = loadJSONFile(jsonFilePath);
        draftToLoad.setDraftName(json.getString(DRAFT_NAME));
        
        loadPlayers(draftToLoad.getPlayers(), json, JSON_PLAYER);
         
        loadFantasyTeams(draftToLoad.getFantasyTeams(), json);
        
        loadProTeams(json, draftToLoad);
        
        
    }
    private void loadProTeams(JsonObject json, Draft draftToLoad){
        JsonArray jsonProTeamsArray = json.getJsonArray(PRO_TEAMS);
        //System.out.print("ProTEams: ");
        for(int i=0; i<jsonProTeamsArray.size(); i++){
            //System.out.println("UI LOADED? jsonFile");
            //System.out.print(" "+jsonProTeamsArray.getString(i));
           draftToLoad.addProTeam(jsonProTeamsArray.getString(i));
        }
        
        
    }
    private void loadFantasyTeams(ObservableList<FantasyTeam> teams, JsonObject json){
        JsonArray jsonTeamsArray = json.getJsonArray(JSON_TEAMS);
        for(int i = 0; i<jsonTeamsArray.size(); i++){
            FantasyTeam team = new FantasyTeam("","");
            JsonObject jso = jsonTeamsArray.getJsonObject(i);
            
            team.setName(jso.getString(TEAM_NAME));
            team.setOwner(jso.getString(TEAM_OWNER));
            team.setPlayerNeeded(Integer.parseInt(jso.getString(JSON_PLAYER_NEED)));
            team.setBudjet(Double.parseDouble(jso.getString(JSON_BUDJET)));
            team.setPP(Integer.parseInt(jso.getString(JSON_PP)));
            team.setR(Integer.parseInt(jso.getString(JSON_R)));
            team.setHR(Integer.parseInt(jso.getString(JSON_HR)));
            team.setRBI(Integer.parseInt(jso.getString(JSON_RBI)));
            team.setSB(Integer.parseInt(jso.getString(JSON_SB)));
            team.setBA(Double.parseDouble(jso.getString(JSON_BA)));
            team.setW(Double.parseDouble(jso.getString(JSON_W)));
            team.setSV(Integer.parseInt(jso.getString(JSON_SV)));
            team.setK(Integer.parseInt(jso.getString(JSON_K)));
            team.setERA(Double.parseDouble(jso.getString(JSON_ERA)));
            team.setWHIP(Double.parseDouble(jso.getString(JSON_WHIP)));
            team.setTotalPoints(Double.parseDouble(jso.getString(JSON_POINTS)));
            loadStartLineUp(team.getPlayers(),  jso.getJsonArray(TEAM_START_LINE));
            loadStartLineUp(team.getTaxiSquad(), jso.getJsonArray(TEAM_TAXI_SQUAD));
            teams.add(team);
        }
        
    }
    private void loadStartLineUp(ObservableList<PlayerPresentor> players, JsonArray startLineUp){
        for (int i = 0; i < startLineUp.size(); i++){
            JsonObject jso = startLineUp.getJsonObject(i);
            
            PlayerPresentor player = new PlayerPresentor();
            player.setAssignedPosition(jso.getString(PLAYER_ASSIG_POS));
            
            player.setContract(jso.getString(PLAYER_CONTRACT ));
            player.setFirstName(jso.getString(PLAYER_FIRSTNAME));
            player.setLastName(jso.getString(PLAYER_LASTNAME));
            player.setNationality(jso.getString(PLAYER_NATION));
            String fanTeamName = jso.getString(FANTASY_TEAM);
            FantasyTeam fantasyTeam = new FantasyTeam(fanTeamName,"");  
           
            
            /*for(int j = 0; j<draftToLoad.getFantasyTeams().size(); j++){
                if(draftToLoad.getFantasyTeams().get(j).getName().equals(fanTeamName))
                    fantasyTeam = draftToLoad.getFantasyTeams().get(j);
            }*/
            player.setFantasyTeam(fantasyTeam);
            player.setBirthYear(Integer.parseInt(jso.getString(PLAYER_BIRTH )));
            
            player.setBAWHIP(Double.parseDouble(jso.getString(PLAYER_BAWHIP)));
            
            player.setContract(jso.getString(PLAYER_CONTRACT));
            
           
            player.setSBERA(Double.parseDouble(jso.getString(PLAYER_SBERA)));
             
            player.setHRSV(Integer.parseInt(jso.getString(PLAYER_HRSV)));
           
            player.setNotes(jso.getString(PLAYER_NOTES));
            player.setPosition(jso.getString(PLAYER_POSITIONS));
            player.setRBIK(Integer.parseInt(jso.getString(PLAYER_RBIK)));
            player.setRW(Integer.parseInt(jso.getString(PLAYER_RW)));
            players.add(player);
            
        }
        
        //return player;
    
    }
    private void loadPlayers(ObservableList<PlayerPresentor> players, JsonObject json, String pathArray){
        JsonArray jsonPlayersArray = json.getJsonArray(pathArray);
        for (int i = 0; i < jsonPlayersArray.size(); i++){
            JsonObject jso = jsonPlayersArray.getJsonObject(i);
            
            PlayerPresentor player = new PlayerPresentor();
            player.setAssignedPosition(jso.getString(PLAYER_ASSIG_POS));
            
            player.setContract(jso.getString(PLAYER_CONTRACT ));
            player.setFirstName(jso.getString(PLAYER_FIRSTNAME));
            player.setLastName(jso.getString(PLAYER_LASTNAME));
            player.setNationality(jso.getString(PLAYER_NATION));
            
            player.setBirthYear(Integer.parseInt(jso.getString(PLAYER_BIRTH )));
            
            player.setBAWHIP(Double.parseDouble(jso.getString(PLAYER_BAWHIP)));
            
            player.setContract(jso.getString(PLAYER_CONTRACT));
            
            player.setTeam(jso.getString(PLAYER_PRO_TEAM));
            player.setSBERA(Double.parseDouble(jso.getString(PLAYER_SBERA)));
             
            player.setHRSV(Integer.parseInt(jso.getString(PLAYER_HRSV)));
           
            player.setNotes(jso.getString(PLAYER_NOTES));
            player.setPosition(jso.getString(PLAYER_POSITIONS));
            player.setRBIK(Integer.parseInt(jso.getString(PLAYER_RBIK)));
            player.setRW(Integer.parseInt(jso.getString(PLAYER_RW)));
            players.add(player);
            
        }
        
        //return player;
    }
    
    @Override
    public ArrayList<Pitcher> loadPitchers(Draft draftToLoad, String jsonFilePath) throws IOException{
       JsonObject json = loadJSONFile(jsonFilePath);
       
      
       JsonArray jsonPitchersArray = json.getJsonArray(JSON_PITCHERS);
        for (int i = 0; i < jsonPitchersArray.size(); i++){
            Pitcher pit = new Pitcher();
            String team = jsonPitchersArray.getJsonObject(i).getString(JSON_TEAM);
            String lastName = jsonPitchersArray.getJsonObject(i).getString(JSON_LAST_NAME);
            String firstName = jsonPitchersArray.getJsonObject(i).getString(JSON_FIRST_NAME);
            String IP = jsonPitchersArray.getJsonObject(i).getString(JSON_IP);
            String ER = jsonPitchersArray.getJsonObject(i).getString(JSON_ER);
            String W = jsonPitchersArray.getJsonObject(i).getString(JSON_W);
            String SV = jsonPitchersArray.getJsonObject(i).getString(JSON_SV);
            String H = jsonPitchersArray.getJsonObject(i).getString(JSON_H);
            String BB = jsonPitchersArray.getJsonObject(i).getString(JSON_BB);
            String K = jsonPitchersArray.getJsonObject(i).getString(JSON_K);
            String notes = jsonPitchersArray.getJsonObject(i).getString(JSON_NOTES);
            String birthYear = jsonPitchersArray.getJsonObject(i).getString(JSON_YEAR_OF_BIRTH);
            String nationality = jsonPitchersArray.getJsonObject(i).getString(JSON_NATION_OF_BIRTH);
            pit.setLastName(lastName);
            pit.setPosition(JSON_P);
            pit.setFistName(firstName);
            pit.setNationality(nationality);
            pit.setTeam(team);
            pit.setIP(IP);
            pit.setER(ER);
            pit.setW(W);
           
            pit.setSV(SV);
            pit.setBB(BB);
            pit.setK(K);
            pit.setH(H);
            pit.setNotes(notes);
            pit.setNationality(nationality);
            pit.setBirthYear(Integer.parseInt(birthYear));
            if(teams.contains(team)==false){
                teams.add(team);
                draftToLoad.addProTeam(team);
            }
            draftToLoad.addPitcher(pit);
            pitchers.add(pit);
            
        }
        
        
        return pitchers;
    }

    /**
     *
     * @param draftToLoad
     * @param jsonFilePath
     * @return
     * @throws IOException
     */
    @Override
    public ArrayList<Hitter> loadHitters(Draft draftToLoad, String jsonFilePath) throws IOException{
       JsonObject json = loadJSONFile(jsonFilePath);
       
      
       JsonArray jsonPitchersArray = json.getJsonArray(JSON_HITTERS);
        for (int i = 0; i < jsonPitchersArray.size(); i++){
            Hitter hit = new Hitter();
            String team = jsonPitchersArray.getJsonObject(i).getString(JSON_TEAM);
            String LastName = jsonPitchersArray.getJsonObject(i).getString(JSON_LAST_NAME);
            String firstName = jsonPitchersArray.getJsonObject(i).getString(JSON_FIRST_NAME);
            String position = jsonPitchersArray.getJsonObject(i).getString(JSON_QP);
            String AB = jsonPitchersArray.getJsonObject(i).getString(JSON_AB);
            String R = jsonPitchersArray.getJsonObject(i).getString(JSON_R);
            String HR = jsonPitchersArray.getJsonObject(i).getString(JSON_HR);
            String H = jsonPitchersArray.getJsonObject(i).getString(JSON_H);
            String RBI = jsonPitchersArray.getJsonObject(i).getString(JSON_RBI);
            String SB = jsonPitchersArray.getJsonObject(i).getString(JSON_SB);
            String notes = jsonPitchersArray.getJsonObject(i).getString(JSON_NOTES);
            String birthYear = jsonPitchersArray.getJsonObject(i).getString(JSON_YEAR_OF_BIRTH);
            String nationality = jsonPitchersArray.getJsonObject(i).getString(JSON_NATION_OF_BIRTH);
            hit.setLastName(LastName);
            hit.setPosition(position);
            hit.setFistName(firstName);
            hit.setNationality(nationality);
            hit.setTeam(team);
            
            hit.setAB(Integer.parseInt(AB));
            hit.setR(Integer.parseInt(R));
           // hit.setRW(Integer.parseInt(R));
            hit.setH(Integer.parseInt(H));
            hit.setRBI(Integer.parseInt(RBI));
            hit.setSB(Integer.parseInt(SB));
            hit.setHR(Integer.parseInt(HR));
            hit.setNotes(notes);
            hit.setNationality(nationality);
            hit.setBirthYear(Integer.parseInt(birthYear));
            if(teams.contains(team)==false){
                teams.add(team);
                draftToLoad.addProTeam(team);
            }
            
            hitters.add(hit);
            draftToLoad.addHitter(hit);
        }
        
        
        return hitters;
    }
    public ArrayList<String> getTeams(){
        return teams;
    }
    public ArrayList<Pitcher> getPitchers(){
        return pitchers;
    }
    public ArrayList<Hitter> getHitters(){
        return hitters;
    }
    private ArrayList<String> loadArrayFromJSONFile(String jsonFilePath, String arrayName) throws IOException {
        JsonObject json = loadJSONFile(jsonFilePath);
        ArrayList<String> items = new ArrayList();
        JsonArray jsonArray = json.getJsonArray(arrayName);
        for (JsonValue jsV : jsonArray) {
            items.add(jsV.toString());
        }
        return items;
    }
    private JsonObject loadJSONFile(String jsonFilePath) throws IOException {
        InputStream is = new FileInputStream(jsonFilePath);
        JsonReader jsonReader = Json.createReader(is);
        JsonObject json = jsonReader.readObject();
        jsonReader.close();
        is.close();
        return json;
    }  

    private JsonArray makePLayersJsonArray(ObservableList<PlayerPresentor> players) {
        JsonArrayBuilder jsb = Json.createArrayBuilder();
        for (PlayerPresentor cP : players) {
           jsb.add(makePlayerJsonObject(cP));
        }
        JsonArray jA = jsb.build();
        return jA; 
        
    }

    private JsonArray makeFantasyTeamJsonArray(ObservableList<FantasyTeam> fantasyTeams) {
        JsonArrayBuilder jsb = Json.createArrayBuilder();
        for (FantasyTeam cP : fantasyTeams) {
            if(cP.getName()!= "Free Agent")
           jsb.add(makeTeamJsonObject(cP));
        }
        JsonArray jA = jsb.build();
        return jA; 
    }
    private JsonArray makeProTeamJsonArray(ObservableList<String> proTeams) {
        JsonArrayBuilder jsb = Json.createArrayBuilder();
        for (String cP : proTeams) {
           jsb.add(cP.toString());
        }
        JsonArray jA = jsb.build();
        return jA;        
    }
     private JsonObject makePlayerJsonObject(PlayerPresentor lecture) {
        JsonObject jso = (JsonObject) Json.createObjectBuilder().add(PLAYER_ASSIG_POS, ""+lecture.getAssignedPosition())
                                            .add(PLAYER_FIRSTNAME, lecture.getFirstName()+"")
                                            .add(PLAYER_LASTNAME, ""+lecture.getLastName())                
                                            .add(PLAYER_BIRTH, ""+lecture.getBirthYear()) 
                                            .add(PLAYER_NATION, ""+lecture.getNationality())
                                            .add(PLAYER_BAWHIP,""+ lecture.getBAWHIP())
                                            .add(PLAYER_CONTRACT,""+ lecture.getContract())
                                            .add(PLAYER_SBERA, ""+lecture.getSBERA())
                                            .add(FANTASY_TEAM, ""+lecture.getFantasyTeam())
                                            .add(PLAYER_HR, ""+lecture.getHR())
                                            .add(PLAYER_HRSV, ""+lecture.getHRSV())
                                            .add(PLAYER_PRO_TEAM, ""+lecture.getTeam())
                                            .add(PLAYER_NOTES,""+lecture.getNotes())
                                            .add(PLAYER_POSITIONS, ""+lecture.getPosition())
                                            .add(PLAYER_RBIK, ""+lecture.getRBIK())
                                            .add(PLAYER_RW, ""+lecture.getRW())
                                    .build();
        
        return jso;
    }
    private JsonObject makeTeamJsonObject(FantasyTeam lecture) {
        JsonObject jso = (JsonObject) Json.createObjectBuilder().add(TEAM_NAME, ""+lecture.getName())
                                            .add(TEAM_OWNER, ""+lecture.getOwner())
                                            .add(JSON_PLAYER_NEED, ""+lecture.getPlayerNeeded())
                                            .add(JSON_BUDJET, ""+lecture.getBudjet())
                                            .add(JSON_PP, ""+lecture.getPP())
                                            .add(JSON_R, ""+lecture.getR())
                                            .add(JSON_HR, ""+lecture.getHR())
                                            .add(JSON_RBI, ""+lecture.getRBI())
                                            .add(JSON_SB, ""+lecture.getSB())
                                            .add(JSON_BA, ""+lecture.getBA())
                                            .add(JSON_W, ""+lecture.getW())
                                            .add(JSON_SV, ""+lecture.getSV())
                                            .add(JSON_K, ""+lecture.getK())
                                            .add(JSON_ERA, ""+lecture.getERA())
                                            .add(JSON_WHIP, ""+lecture.getWHIP())
                                            .add(JSON_POINTS, ""+lecture.getTotalPoints())
                                            .add(TEAM_START_LINE, makePLayersJsonArray(lecture.getPlayers()))
                                            .add(TEAM_TAXI_SQUAD,makePLayersJsonArray(lecture.getTaxiSquad()))
                                    .build();
        return jso;
    }

    
}
