/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package wdk.controller;

import javafx.collections.ObservableList;
import javafx.stage.Stage;
import properties_manager.PropertiesManager;
import static wdk.WDK_PropertyType.REMOVE_ITEM_MESSAGE;
import wdk.data.Draft;
import wdk.data.DraftDataManager;
import wdk.data.FantasyTeam;
import wdk.data.PlayerPresentor;
import wdk.gui.FantasyTeamDialog;
import wdk.gui.MessageDialog;
import wdk.gui.PlayerEditDialog;
import wdk.gui.PlayerItemDialog;
import wdk.gui.WDK_UI;
import wdk.gui.YesNoCancelDialog;

/**
 *
 * @author Thierno
 */
public class DraftEditController {
   
    FantasyTeamDialog ftd;
    PlayerItemDialog pid;
    PlayerEditDialog ped;
    MessageDialog messageDialog;
    YesNoCancelDialog yesNoCancelDialog;
    private boolean enabled;
    
    public DraftEditController(Stage initPrimaryStage, Draft draft, 
            MessageDialog initMessageDialog, YesNoCancelDialog initYesNoCancelDialog) {
        ftd = new FantasyTeamDialog(initPrimaryStage, draft, initMessageDialog);
        pid = new PlayerItemDialog(initPrimaryStage, draft, initMessageDialog);
        //ped = new PlayerEditDialog(initPrimaryStage, draft, initMessageDialog);
        //lid = new LectureItemDialog(initPrimaryStage, course, initMessageDialog);
        messageDialog = initMessageDialog;
        yesNoCancelDialog = initYesNoCancelDialog;
    }

    // THESE ARE FOR SCHEDULE ITEMS
    
   /* public void handleAddScheduleItemRequest(CSB_GUI gui) {
        CourseDataManager cdm = gui.getDataManager();
        Course course = cdm.getCourse();
        sid.showAddScheduleItemDialog(course.getStartingMonday());
        
        // DID THE USER CONFIRM?
        if (sid.wasCompleteSelected()) {
            // GET THE SCHEDULE ITEM
            ScheduleItem si = sid.getScheduleItem();
            
            // AND ADD IT AS A ROW TO THE TABLE
            course.addScheduleItem(si);
        }
        else {
            // THE USER MUST HAVE PRESSED CANCEL, SO
            // WE DO NOTHING
        }
    }
    public void handleAddAssignment(CSB_GUI gui){
        CourseDataManager cdm = gui.getDataManager();
        Course course = cdm.getCourse();
        aid.showAddAssignmentItemDialog(course.getStartingMonday());
        if (aid.wasCompleteSelected()) {
            // GET THE SCHEDULE ITEM
            //ScheduleItem si = sid.getScheduleItem();
            Assignment hw = aid.getAssignmentItem();
            
            // AND ADD IT AS A ROW TO THE TABLE
            course.addAssignment(hw);
        }
        else {
            // THE USER MUST HAVE PRESSED CANCEL, SO
            // WE DO NOTHING
        } 
    }
    
    public void handleEditScheduleItemRequest(CSB_GUI gui, ScheduleItem itemToEdit) {
        CourseDataManager cdm = gui.getDataManager();
        Course course = cdm.getCourse();
        sid.showEditScheduleItemDialog(itemToEdit);
        
        // DID THE USER CONFIRM?
        if (sid.wasCompleteSelected()) {
            // UPDATE THE SCHEDULE ITEM
            ScheduleItem si = sid.getScheduleItem();
            itemToEdit.setDescription(si.getDescription());
            itemToEdit.setDate(si.getDate());
            itemToEdit.setLink(si.getLink());
        }
        else {
            // THE USER MUST HAVE PRESSED CANCEL, SO
            // WE DO NOTHING
        }        
    }
    public void handleEditAssignmentItemRequest(CSB_GUI gui, Assignment itemToEdit) {
        CourseDataManager cdm = gui.getDataManager();
        Course course = cdm.getCourse();
        aid.showEditAssignmentItemDialog(itemToEdit);
        
        // DID THE USER CONFIRM?
        if (aid.wasCompleteSelected()) {
            // UPDATE THE SCHEDULE ITEM
            Assignment si = aid.getAssignmentItem();
            itemToEdit.setName(si.getName());
            itemToEdit.setDate(si.getDate());
            itemToEdit.setTopics(si.getTopics());
        }
        else {
            // THE USER MUST HAVE PRESSED CANCEL, SO
            // WE DO NOTHING
        }        
    }
    public void handleEditLectureItemRequest(CSB_GUI gui, Lecture itemToEdit) {
        CourseDataManager cdm = gui.getDataManager();
        Course course = cdm.getCourse();
        lid.showEditLectureItemDialog(itemToEdit);
        
        // DID THE USER CONFIRM?
        if (lid.wasCompleteSelected()) {
            // UPDATE THE SCHEDULE ITEM
            Lecture lec = lid.getLectureItem();
            itemToEdit.setTopic(lec.getTopic());
            itemToEdit.setSessions(lec.getSessions());
            //itemToEdit.setLink(si.getLink());
        }
        else {
            // THE USER MUST HAVE PRESSED CANCEL, SO
            // WE DO NOTHING
        }        
    }*/
    public void handleAddPlayer(WDK_UI gui, ObservableList<String> teams){
       DraftDataManager ddm = gui.getDataManager();
        Draft draft = ddm.getDraft();
        pid.showAddPlayerItemDialog(teams);
        if (pid.wasCompleteSelected()) {
            // GET THE SCHEDULE ITEM
            //ScheduleItem si = sid.getScheduleItem();
            PlayerPresentor player = pid.getPlayerItem();
            
            // AND ADD IT AS A ROW TO THE TABLE
            gui.getFileController().markAsEdited(gui);
            
            draft.addPlayerItem(player);
        }
        else {
            // THE USER MUST HAVE PRESSED CANCEL, SO
            // WE DO NOTHING
        } 
    }
    public void handleAddFantasyTeam(WDK_UI gui){
       DraftDataManager ddm = gui.getDataManager();
        Draft draft = ddm.getDraft();
        ftd.showAddFantasyTeamDialog();
        if (ftd.wasCompleteSelected()) {
            // GET THE SCHEDULE ITEM
            //ScheduleItem si = sid.getScheduleItem();
            FantasyTeam team = ftd.getFantasyTeam();
            
            // AND ADD IT AS A ROW TO THE TABLE
            gui.getFileController().markAsEdited(gui);
            draft.addFantasyTeam(team);
           // gui.loadFantasyStandingTable(draft.getFantasyTeamsStanding());
            gui.getFileController().markAsEdited(gui);
        }
        else {
            // THE USER MUST HAVE PRESSED CANCEL, SO
            // WE DO NOTHING
        } 
    }
    public void handleRemoveFantasyTeam(WDK_UI gui, FantasyTeam itemToRemove) {
        // PROMPT THE USER TO SAVE UNSAVED WORK
         yesNoCancelDialog.show(PropertiesManager.getPropertiesManager().getProperty(REMOVE_ITEM_MESSAGE));
        
        // AND NOW GET THE USER'S SELECTION
        String selection = yesNoCancelDialog.getSelection();

        // IF THE USER SAID YES, THEN SAVE BEFORE MOVING ON
        if (selection.equals(YesNoCancelDialog.YES)) { 
            gui.getDataManager().getDraft().removeFantasyTeam(itemToRemove);
            gui.getFileController().markAsEdited(gui);
            //gui.loadFantasyStandingTable(gui.getDataManager().getDraft().getFantasyTeamsStanding());
        }
    
    }
    public void handleRemovePlayerItem(WDK_UI gui, PlayerPresentor itemToRemove) {
        // PROMPT THE USER TO SAVE UNSAVED WORK
         yesNoCancelDialog.show(PropertiesManager.getPropertiesManager().getProperty(REMOVE_ITEM_MESSAGE));
        
        // AND NOW GET THE USER'S SELECTION
        String selection = yesNoCancelDialog.getSelection();

        // IF THE USER SAID YES, THEN SAVE BEFORE MOVING ON
        if (selection.equals(YesNoCancelDialog.YES)) { 
            gui.getDataManager().getDraft().removePlayerItem(itemToRemove);
            gui.getFileController().markAsEdited(gui);
        }
    
    }
    
    public void handleEditFantasyTeam(WDK_UI gui, FantasyTeam itemToEdit) {
        DraftDataManager ddm = gui.getDataManager();
        Draft draft = ddm.getDraft();
        
        ftd.showEditFantasyTeamDialog(itemToEdit);
        
        // DID THE USER CONFIRM?
        if (ftd.wasCompleteSelected()) {
            // UPDATE THE SCHEDULE ITEM
            FantasyTeam team = ftd.getFantasyTeam();
            itemToEdit.setName(team.getName());
            itemToEdit.setOwner(team.getOwner());
            //itemToEdit.setLink(si.getLink());
            gui.getFileController().markAsEdited(gui);
            //gui.loadFantasyStandingTable(draft.getFantasyTeamsStanding());
        }
        else {
            // THE USER MUST HAVE PRESSED CANCEL, SO
            // WE DO NOTHING
        }        
    }

     public void enable(boolean enableSetting) {
        enabled = enableSetting;
    }
     public boolean isEnable(){
         return enabled;
     }
     public void handleSelectPlayerRequest(WDK_UI gui){
         gui.initAutoDraft();
     }
    /**
     * To move up lecture
     * @param gui
     * @param itemToEdit 
     */
   /* public void hanleMoveUpLectureRequest(CSB_GUI gui, Lecture itemToEdit){
         CourseDataManager cdm = gui.getDataManager();
         Course course = cdm.getCourse();
         ObservableList<Lecture> lectures = course.getLectures();
         int index = getIndexOfLecture(lectures, itemToEdit);
         if( index>0){
            lectures.remove(itemToEdit);
            lectures.add(index-1, itemToEdit);
         }
        
    }/**
     * To moveDown Lecture
     * @param gui
     * @param itemToEdit 
     */
   /* public void hanleMoveDownLectureRequest(CSB_GUI gui, Lecture itemToEdit){
         CourseDataManager cdm = gui.getDataManager();
         Course course = cdm.getCourse();
         ObservableList<Lecture> lectures = course.getLectures();
         int index = getIndexOfLecture(lectures, itemToEdit);
         if( index<lectures.size()-1){
            lectures.remove(itemToEdit);
            lectures.add(index+1, itemToEdit);
         }
        
    }
    /**
     * To get the index of the lecture on the table
     * @param lectures
     * @param itemToEdit
     * @return 
     */
    /*private int getIndexOfLecture(ObservableList<Lecture> lectures, Lecture itemToEdit){
        int i=0;
        while(i<lectures.size()){
            if(lectures.get(i).equals(itemToEdit))
                break;
            i++;
        }
        return i;
    }
    /**
     * To removeLecture on the table
     * @param gui
     * @param itemToRemove 
     */
    /*public void handleRemoveLectureItemRequest(CSB_GUI gui, Lecture itemToRemove) {
        // PROMPT THE USER TO SAVE UNSAVED WORK
        yesNoCancelDialog.show(PropertiesManager.getPropertiesManager().getProperty(REMOVE_ITEM_MESSAGE));
        
        // AND NOW GET THE USER'S SELECTION
        String selection = yesNoCancelDialog.getSelection();

        // IF THE USER SAID YES, THEN SAVE BEFORE MOVING ON
        if (selection.equals(YesNoCancelDialog.YES)) { 
            gui.getDataManager().getCourse().removeLecture(itemToRemove);
        }
    }
    public void handleRemoveAssignmentItemRequest(CSB_GUI gui, Assignment itemToRemove) {
        // PROMPT THE USER TO SAVE UNSAVED WORK
        yesNoCancelDialog.show(PropertiesManager.getPropertiesManager().getProperty(REMOVE_ITEM_MESSAGE));
        
        // AND NOW GET THE USER'S SELECTION
        String selection = yesNoCancelDialog.getSelection();

        // IF THE USER SAID YES, THEN SAVE BEFORE MOVING ON
        if (selection.equals(YesNoCancelDialog.YES)) { 
            gui.getDataManager().getCourse().removeAssignment(itemToRemove);
        }
    }
    
    public void handleRemoveScheduleItemRequest(CSB_GUI gui, ScheduleItem itemToRemove) {
        // PROMPT THE USER TO SAVE UNSAVED WORK
        yesNoCancelDialog.show(PropertiesManager.getPropertiesManager().getProperty(REMOVE_ITEM_MESSAGE));
        
        // AND NOW GET THE USER'S SELECTION
        String selection = yesNoCancelDialog.getSelection();

        // IF THE USER SAID YES, THEN SAVE BEFORE MOVING ON
        if (selection.equals(YesNoCancelDialog.YES)) { 
            gui.getDataManager().getCourse().removeScheduleItem(itemToRemove);
        }
    }*/
    
}