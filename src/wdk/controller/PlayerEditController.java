/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package wdk.controller;

import java.util.ArrayList;
import javafx.collections.ObservableList;
import javafx.stage.Stage;
import wdk.data.Draft;
import wdk.data.DraftDataManager;
import wdk.data.FantasyTeam;
import wdk.data.PlayerPresentor;
import wdk.gui.FantasyTeamDialog;
import wdk.gui.MessageDialog;
import wdk.gui.PlayerEditDialog;
import wdk.gui.PlayerItemDialog;
import wdk.gui.WDK_UI;
import wdk.gui.YesNoCancelDialog;

/**
 *
 * @author Thierno
 */
public class PlayerEditController {
    PlayerEditDialog ped;
    private final MessageDialog messageDialog;
    private final YesNoCancelDialog yesNoCancelDialog;
    public PlayerEditController(Stage initPrimaryStage, PlayerPresentor draft, 
            MessageDialog initMessageDialog, YesNoCancelDialog initYesNoCancelDialog) {
       
        ped = new PlayerEditDialog(initPrimaryStage, draft, initMessageDialog);
        //lid = new LectureItemDialog(initPrimaryStage, course, initMessageDialog);
        messageDialog = initMessageDialog;
        yesNoCancelDialog = initYesNoCancelDialog;
    }
    /**
     * 
     * @param gui
     * @param itemToEdit
     * @param fantasyTeams
     * @param pos 
     */
    public void handleEditPlayer(WDK_UI gui, PlayerPresentor itemToEdit, 
            ObservableList<FantasyTeam> fantasyTeams) {
        DraftDataManager ddm = gui.getDataManager();
        Draft draft = ddm.getDraft();
        //ped = new PlayerEditDialog(initPrimaryStage, itemToEdit, initMessageDialog);
        FantasyTeam t = itemToEdit.getFantasyTeam();
        ped.showEditPlayerDialog(itemToEdit,fantasyTeams);
        
        // DID THE USER CONFIRM?
        if (ped.wasCompleteSelected()) {
            // UPDATE THE SCHEDULE ITEM
            PlayerPresentor pl = ped.getPlayerItem();
            itemToEdit.setFirstName(pl.getFirstName());
            itemToEdit.setLastName(pl.getLastName());
            itemToEdit.setNationality(pl.getNationality());
            itemToEdit.setAssignedPosition(pl.getAssignedPosition());
            itemToEdit.setNationality(pl.getNationality());
            itemToEdit.setSalary(pl.getSalary());
            itemToEdit.setContract(pl.getContract());
            if(pl.getFantasyTeam().getPlayers().contains(pl)==false)
             pl.getFantasyTeam().addPlayer(pl);
              t.removePlayer(pl);
               //gui.loadFantasyStandingTable(draft.getFantasyTeamsStanding());
            if(pl.getFantasyTeam().getName().equals("Free Agent")){
                if(draft.getPlayers().contains(pl)==false)
                   draft.getPlayers().add(pl);
                // gui.loadFantasyStandingTable(draft.getFantasyTeamsStanding());
            }
            else{
                draft.getPlayers().remove(pl);
                 //gui.loadFantasyStandingTable(draft.getFantasyTeamsStanding());
            }
           
            gui.getFileController().markAsEdited(gui);
            //itemToEdit.setLink(si.getLink());
        }
        else {
            // THE USER MUST HAVE PRESSED CANCEL, SO
            // WE DO NOTHING
        }        
    }
}
