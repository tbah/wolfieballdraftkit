/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package wdk.data;

import java.text.DecimalFormat;
import java.text.NumberFormat;
import javafx.beans.property.DoubleProperty;
import javafx.beans.property.IntegerProperty;
import javafx.beans.property.SimpleDoubleProperty;
import javafx.beans.property.SimpleIntegerProperty;
import javafx.beans.property.SimpleStringProperty;
import javafx.beans.property.StringProperty;

/**
 *
 * @author Thierno
 */
public class PlayerPresentor {
    StringProperty lastName;
    StringProperty firstName;
    StringProperty team;
    NumberFormat formatter;
    StringProperty position;
    IntegerProperty birthYear;
    StringProperty assignedPosition;
    IntegerProperty rw;
    IntegerProperty hrsv;
    IntegerProperty rbik;
    DoubleProperty sbera;
    DoubleProperty bawhip;
    StringProperty notes;
    DoubleProperty salary;
    StringProperty nationality;
    StringProperty contract;
    FantasyTeam fantasyTeam;
    IntegerProperty order;
    IntegerProperty h;
    DoubleProperty ip ;
    IntegerProperty er;
    IntegerProperty ab;
    
    //ObservableList<PlayerPresentor> players;
    
   
   
    public PlayerPresentor(Pitcher pit, Hitter hit, int num) {
       /* rw = new SimpleIntegerProperty();
        hrsv = new SimpleIntegerProperty();
        rw = new SimpleIntegerProperty();
        rbik = new SimpleIntegerProperty();
        sbera = new SimpleDoubleProperty();
        bawhip = new SimpleDoubleProperty();*/
        if (num == 1){
         initPitcher(pit);  
        }else{
            initHitter(hit);
        } 
        formatter = new DecimalFormat("#0.000"); 
    }

    public PlayerPresentor() {
        firstName = new SimpleStringProperty();
        lastName = new SimpleStringProperty();
        position = new SimpleStringProperty();
        nationality = new SimpleStringProperty();
        team = new SimpleStringProperty();
        
        //positions = new SimpleStringProperty();
        birthYear = new SimpleIntegerProperty();
        notes = new SimpleStringProperty();
        rw = new SimpleIntegerProperty();
        hrsv = new SimpleIntegerProperty();
        rw = new SimpleIntegerProperty();
        rbik = new SimpleIntegerProperty();
        sbera = new SimpleDoubleProperty();
        bawhip = new SimpleDoubleProperty();
        salary = new SimpleDoubleProperty();
        contract = new SimpleStringProperty();
        fantasyTeam = new FantasyTeam("","");
        assignedPosition = new SimpleStringProperty();
        order = new SimpleIntegerProperty();
        formatter = new DecimalFormat("#0.###"); 
        h = new SimpleIntegerProperty();
        ip = new SimpleDoubleProperty();
        er = new SimpleIntegerProperty();
        ab = new SimpleIntegerProperty();
    }
   /* public ObservableList<PlayerPresentor> getPlayers(){
        return players;
    }*/
    public StringProperty getLastNameProperty(){
        return lastName;
    }
    public void setOrder(int num){
        order.set(num);
    }
    public int getOrder(){
        return order.get();
    }
    public IntegerProperty getOrderProperty(){
        return order;
    }
    public String getLastName(){
        return lastName.get();
    }
    public StringProperty getFirstNameProperty(){
        return firstName;
    }
    public String getFirstName(){
        return firstName.get();
    }
    public void setFirstName(String name){
        firstName.set(name);
    }
    public void setLastName(String name){
        lastName.set(name);
    }
    public StringProperty getTeamProperty(){
        return team;
    }
    public String getTeam(){
        return team.get();
    }
    public StringProperty getPositionProperty(){
        return position;
    }
    public String getPosition(){
        return position.get();
    }
    public IntegerProperty getBirthYearProperty(){
        return birthYear;
    }
    public int getBirthYear(){
        return birthYear.get();
    }
    public IntegerProperty getRProperty(){
        return rw;
    }
     public IntegerProperty getWProperty(){
        return rw;
    }
     public IntegerProperty rwProperty(){
        return rw;
    }
    public Integer getRW(){
        return rw.get();
    }
    public Integer getW(){
        return rw.get();
    }
    public IntegerProperty hrsvProperty(){
        return hrsv;
    }
    public Integer getHRSV(){
        return hrsv.get();
    }
    public Integer getHR(){
        return hrsv.get();
    }
    public IntegerProperty rbikProperty(){
        return rbik;
    }
    public Integer getRBIK(){
        return rbik.get();
    }
    public Integer getSV(){
        return hrsv.get();
    }
    public DoubleProperty sberaProperty(){
        return sbera;
    }
    public double getSBERA(){
        return sbera.get();
    }
    public double getERA(){
        return sbera.get();
    }
    public DoubleProperty bawhipProperty(){
        return bawhip;
    } 
    public Double getBAWHIP(){
        
        return bawhip.get();
    }
    public String getNotes(){
        return notes.get();
    }
    public StringProperty getNotesProperty(){
        return notes;
    } 
    public void setAssignedPosition(String pos){
        assignedPosition.set(pos);
    }
    public String getAssignedPosition(){
        return assignedPosition.get();
    }
    public StringProperty getAssignedPositionProperty(){
        return assignedPosition;
    }
    private void initPitcher(Pitcher pit){
        
        lastName = pit.getLastNameProperty();
        firstName = pit.getFirstNameProperty();
        team = pit.getTeamPropertype();
        position = pit.getPositionsProperty();
        birthYear = pit.getBirthYearProperty();
        salary = pit.getSalaryProperty();
        rw = pit.rwProperty();
        nationality = pit.getNationalityPropertyType();
        //System.out.println(rw.get());
        hrsv = pit.hrsvProperty();
        rbik = pit.rbikProperty();        
        sbera = pit.sberaPrperty();
       // System.out.println("sbera pit = "+sbera.get());
        bawhip = pit.bawhipProperty();//.getWHIP();
        //System.out.println("bawhip pit = "+bawhip.get());
        notes = pit.getNotesProperty(); 
        contract = pit.getContractProperty();
        fantasyTeam = pit.getFantasyTeam();
        assignedPosition = pit.getAssignedPositionProperty();
        order  = pit.getOrderProperty();
        h = pit.h;
        ip = pit.getIP();
        er = pit.getERProperty();
        ab = new SimpleIntegerProperty();
        ab.set(0);
    }
    private void initHitter(Hitter hit){
        lastName = hit.getLastNameProperty();
        firstName = hit.getFirstNameProperty();
        team = hit.getTeamPropertype();
        position = hit.getPositionsProperty();
        birthYear = hit.getBirthYearProperty();
        System.out.println(" "+hit.getR());
        salary = hit.getSalaryProperty();
        rw = hit.rwProperty();
       // System.out.println(rw);
        nationality = hit.getNationalityPropertyType();
        hrsv = hit.hrsvProperty();
        rbik = hit.rbikProperty();//getRBIProperty();        
        sbera = hit.sberaPrperty();//getSBProperty();
        //System.out.println("sbera Hit = "+sbera.get());
        bawhip = hit.bawhipProperty();//getBAProperty();
        //System.out.println("bawhip hit = "+bawhip.get());
        notes = hit.getNotesProperty();  
        contract = hit.getContractProperty();
        fantasyTeam = hit.getFantasyTeam();
        assignedPosition = hit.getAssignedPositionProperty();
        h = hit.h;
        order  = hit.getOrderProperty();
        ip = new SimpleDoubleProperty();
        ip.set(0);
        er = new SimpleIntegerProperty();
        er.set(0);
        ab = hit.getABProperty();
    }

    public IntegerProperty getERProperty(){
        return er;
    }
    public int getER(){
        return er.get();
    }
    public void setTeam(String team) {
        this.team.set(team);
    }
    public void addPosition(String pos){
        String posit = position.get();
        if(posit==null)
            posit = pos;
        else
            posit +="_"+pos;
        
        position.set(posit);
    }

    public void setSalary(String newValue) {
        salary.set(Double.parseDouble(newValue));
    }
    public void setSalary(double newValue) {
        salary.set(newValue);
    }
    public Double getSalary(){
        return salary.get();
    }

    public String getNationality() {
       return nationality.get();
    }
    public void setNationality(String nation){
        nationality.set(nation);
    }
    public void setContract(String str){
        contract.set(str);
    }
    public String getContract(){
        return contract.get();
    }
    public StringProperty getContractProperty(){
        return contract;
    }
    public void setFantasyTeam(FantasyTeam team){
        fantasyTeam = team;
    }
    public FantasyTeam getFantasyTeam(){
        return fantasyTeam;
    }
     public void setBirthYear(int year){
        birthYear.set(year);
    }
    public void setBAWHIP(double num){
        bawhip.set(Double.parseDouble(formatter.format(num)));
        
    }
    public void setSBERA(double num){
        sbera.set(num);
    }
    public void setRBIK(int num){
        rbik.set(num);
    }
    public void setHRSV(int num){
        hrsv.set(num);
    }
    public void setRW(int num){
        rw.set(num);
    }
    public void setNotes(String name){
        notes.set(name);
    }
    public void setPosition(String name){
        position.set(name);
    }
    @Override
    public String toString(){
        return firstName.get()+" "+lastName.get();
    }
    public int getH(){
        return h.get();
    }
    public void setH(int n){
        h.set(n);
    }
    public IntegerProperty getHProperty(){
        return h;
    }
    public void setIP(double n){
        ip.set(n);
    }
    public DoubleProperty getIPPrperty(){
        return ip;
    }
    public double getIP(){
        return ip.get();
    }
    public IntegerProperty getABProperty(){
        return ab;
    }
    public int getAB(){
        return ab.get();
    }
}

