/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package wdk.data;

import javafx.beans.property.DoubleProperty;
import javafx.beans.property.IntegerProperty;
import javafx.beans.property.SimpleDoubleProperty;
import javafx.beans.property.SimpleIntegerProperty;
import javafx.beans.property.StringProperty;

/**
 *
 * @author Thierno
 */
public class Pitcher extends Player {
    boolean isPitcher = true;
    DoubleProperty ip ;
    IntegerProperty er;
    IntegerProperty w;
    IntegerProperty sv;
    IntegerProperty h;
    IntegerProperty bb;
    IntegerProperty k;
    DoubleProperty ERA;
    private DoubleProperty whip;
    public Pitcher(){
        super();
        ip = new SimpleDoubleProperty();
        er = new SimpleIntegerProperty();
        w = new SimpleIntegerProperty();
        sv = new SimpleIntegerProperty();
        h = new SimpleIntegerProperty();
        bb = new SimpleIntegerProperty();
        k = new SimpleIntegerProperty();
       ERA = new SimpleDoubleProperty();
       whip = new SimpleDoubleProperty();
    }
    public void setIP(String num){
        
        ip.set(Double.parseDouble(num));
    }
    public void setER(String num){
        er.set(Integer.parseInt(num));
    }
    public void setW(String num){
        w.set(Integer.parseInt(num));
        super.rw.set(Integer.parseInt(num));
    }
    public void setSV(String num){
        sv.set(Integer.parseInt(num));
        super.hrsv.set(Integer.parseInt(num));
    }
    /*public Integer getRW(){
        return super.getRW();
    }*/
    public void setH(String num){
        h.set(Integer.parseInt(num));
    }
    public void setBB(String num){
        bb.set(Integer.parseInt(num));
    }
    public void setK(String num){
        k.set(Integer.parseInt(num));
        super.rbik.set(Integer.parseInt(num));
    }
    public int getER(){
        return er.get();
    }
    public IntegerProperty getERProperty(){
        return er;
    }
    public IntegerProperty getWProperty(){
        return w;
    }
    public int getW(){
        return w.get();
    }
    public int getSV(){
        return sv.get();
    }
     public IntegerProperty getSVProperty(){
        return sv;
    }
    public int getH(){
        return h.get();
    }
    public IntegerProperty getHProperty(){
        return h;
    }
    public int getBB(){
        return bb.get();
    }
    public IntegerProperty getBBroperty(){
        return bb;
    }
    public IntegerProperty getKProperty(){
        return k;
    }
    public int getk(){
        return k.get();
    }
    public DoubleProperty getIP(){
        return ip;
    }
    public DoubleProperty getWHIP(){
        double p = 0.0;
        if(ip.get()>0)
        p = (w.get()+h.get())/ip.get();
        p = Double.parseDouble(formatter.format(p));
        whip.set(p);
        
        super.setBAWHIP(p);//;bawhip.set(p);
        return whip;
    }
    @Override
    public DoubleProperty bawhipProperty(){
       double p = 0.0;
        if(ip.get()>0)
        p = (w.get()+h.get())/ip.get();
        p = Double.parseDouble(formatter.format(p));
        whip.set(p);
        super.setBAWHIP(p);//.bawhip.set(p);
        return whip;
    }
    @Override
    public Double getBAWHIP(){
        return super.bawhip.get();
    }
    
    @Override
    public DoubleProperty sberaProperty(){
       double p = 0.0;
        if(ip.get()>0)
            p = er.get()*9/ip.get();
        ERA.set(p);
        super.sbera.set(p);
        return ERA;
    }
    @Override
    public Double getSBERA(){
        return ERA.get();
    }

    
}
