/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package wdk.data;

import java.text.DecimalFormat;
import java.text.NumberFormat;
import javafx.beans.property.DoubleProperty;
import javafx.beans.property.IntegerProperty;
import javafx.beans.property.SimpleDoubleProperty;
import javafx.beans.property.SimpleIntegerProperty;
import javafx.beans.property.SimpleStringProperty;
import javafx.beans.property.StringProperty;
import javafx.collections.ObservableList;

/**
 *
 * @author Thierno
 */
public class Player {
    StringProperty assignedPosition;
    StringProperty firstName;
    StringProperty lastName;
    DoubleProperty salary;
    StringProperty nationality;
    IntegerProperty birthYear;
    StringProperty contract;
    StringProperty team;
    StringProperty positions;
    StringProperty notes;
    FantasyTeam fantasyTeam;
    IntegerProperty rw;
    IntegerProperty hrsv;
    IntegerProperty rbik;
    DoubleProperty sbera;
    DoubleProperty bawhip;
    NumberFormat formatter;
   // StringProperty notes;
    //ObservableList<PlayerPresentor> players
    IntegerProperty order;
    
    public Player(){
        firstName = new SimpleStringProperty();
        lastName = new SimpleStringProperty();
        nationality = new SimpleStringProperty();
        team = new SimpleStringProperty();
        positions = new SimpleStringProperty();
        birthYear = new SimpleIntegerProperty();
        notes = new SimpleStringProperty();
        salary = new SimpleDoubleProperty();
        rw = new SimpleIntegerProperty();
        hrsv = new SimpleIntegerProperty();
        rw = new SimpleIntegerProperty();
        rbik = new SimpleIntegerProperty();
        sbera = new SimpleDoubleProperty();
        bawhip = new SimpleDoubleProperty();
        contract = new SimpleStringProperty();
        fantasyTeam = new FantasyTeam("", "");  
        assignedPosition = new SimpleStringProperty();
        order = new SimpleIntegerProperty();
        formatter = new DecimalFormat("#0.##"); 
    }
    public void setContract(String str){
        contract.set(str);
    }
    public String getContract(){
        return contract.get();
    }
    public StringProperty getContractProperty(){
        return contract;
    }
    public void setFistName(String name){
        firstName.set(name);
    }
    public void setLastName(String name){
        lastName.set(name);
    }
    public void setNationality(String name){
        nationality.set(name);
    }
    public void setPosition(String name){
        positions.set(name);
    }
    public void setNotes(String name){
        notes.set(name);
    }
    public void setTeam(String team){
        this.team.set(team);
    }
    public StringProperty getTeamPropertype(){
        return team;
    }
    public void setRW(int num){
        rw.set(num);
    }
    public IntegerProperty rwProperty(){
        return rw;
    }
    public void setHRSV(int num){
        hrsv.set(num);
    }
    public Integer getHRSV(){
        return hrsv.get();
    }
    public IntegerProperty hrsvProperty(){
        return hrsv;
    }
    public Integer getRW(){
        return rw.get();
    }
    public void setRBIK(int num){
        rbik.set(num);
    }
    public IntegerProperty rbikProperty(){
        return rbik;
    }
    public Integer rbik(){
        return rbik.get();
    }
    public DoubleProperty getSalaryProperty(){
        return salary;
    }
    public void setSalary(double sala){
        salary.set(sala);
    }
    public void setSBERA(double num){
        sbera.set(num);
    }
    public Double getSBERA(){
       return sbera.get();
    }
    public DoubleProperty sberaPrperty(){
       return sbera;
    }
    public void setBAWHIP(double num){
        
       bawhip.set(Double.parseDouble(formatter.format(num)));
        
    }
    public Double getBAWHIP(){
       bawhip.set(Double.parseDouble(formatter.format(bawhip.get())));
        return bawhip.get();
    }
    public DoubleProperty bawhipProperty(){
        bawhip.set(Double.parseDouble(formatter.format(bawhip.get())));
       return bawhip;
    }
    public DoubleProperty sberaProperty(){
        return sbera;
    }
    public String getTeam(){
        return team.get();
    }
    public StringProperty getNotesProperty(){
        return notes;
    }
    public String getNotes(){
        return notes.get();
    }
    public void setBirthYear(int year){
        birthYear.set(year);
    }
    public String getFirstName(){
        return firstName.get();
    }
    public StringProperty getFirstNameProperty(){
        return firstName;
    }
    public StringProperty getLastNameProperty(){
        return lastName;
    }
    public String getLastName(){
        return lastName.get();
    }
    public StringProperty getNationalityPropertyType(){
        return nationality;
    }
    public String getNationality(){
        return nationality.get();
    }
    public StringProperty getPositionsProperty(){
        return positions;
    }
     public IntegerProperty getBirthYearProperty(){
        return birthYear;
    }
    public void setFantasyTeam(FantasyTeam team){
        fantasyTeam = team;
    }
    public FantasyTeam getFantasyTeam(){
        return fantasyTeam;
    }
    public void setAssignedPosition(String pos){
        assignedPosition.set(pos);
    }
    public StringProperty getAssignedPositionProperty() {
       return assignedPosition;
    }
    public void setOrder(int num){
        order.set(num);
    }
    public int getOrder(){
        return order.get();
    }
    public IntegerProperty getOrderProperty(){
        return order;
    }
}
