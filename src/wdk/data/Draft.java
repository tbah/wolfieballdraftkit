/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package wdk.data;

import java.util.Collections;
import java.util.Comparator;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;

/**
 *
 * @author Thierno
 */
public class Draft {
    ObservableList<Pitcher> pitchers;
    ObservableList<Hitter> hitters;
    ObservableList<PlayerPresentor> players;
    ObservableList<FantasyTeam> fantasyTeams;
    ObservableList<FantasyTeam> fteams;
    ObservableList<String> proTeams;
    FantasyTeam freeAgent;
    String draftName ;
    public Draft(){
        pitchers = FXCollections.observableArrayList();
        hitters = FXCollections.observableArrayList();
        players = FXCollections.observableArrayList();
        fantasyTeams = FXCollections.observableArrayList();
        proTeams = FXCollections.observableArrayList();
        freeAgent = new FantasyTeam("Free Agent","");
        freeAgent.setAll(players);
        fantasyTeams.add(0,freeAgent);
        fteams = FXCollections.observableArrayList();
        
    }
    public ObservableList<FantasyTeam> getFantasyTeamsStanding(){
       //ObservableList<FantasyTeam> fteams = FXCollections.observableArrayList();
       //if(fantasyTeams.size()>2) 
       fteams.setAll(fantasyTeams) ;
       fteams.remove(0);
       
        return fteams;
    }
    public void calculatePoints(){
       // ObservableList<FantasyTeam> teams =getFantasyTeamsStanding();
        // To reset before calculation 
        for(int i=0;i<getFantasyTeamsStanding().size();i++){
            fteams.get(i).setTotalPoints(0);
        }
        assignPoints();
        
        Collections.sort(fteams, new CompareByPoints());
    }
    private void assignPoints(){
        for(int i = 1; i<11; i++){
            FantasyTeam [] teams = insertionSort(getFantasyTeamsStanding(), i);
            int n = teams.length;
            int lower = 1;
            for(int j=0; j<teams.length; j++){
             if(i<9){
             teams[j].addPoint(n);
              n--;
             }else{
                teams[j].addPoint(lower);
                lower++;
             }
            }
            
                
            
        }
    }
    private FantasyTeam []  insertionSort(ObservableList<FantasyTeam> teams, int n)
      {
      
      int in, out;
      int nElems = teams.size();
      FantasyTeam [] a = new FantasyTeam [nElems];
      teams.toArray(a);
      switch(n){
          case 1:
              for(out=1; out<nElems; out++)     // out is dividing line
                {
                int temp = a[out].getR();            // remove marked item
                FantasyTeam t = a[out];
                in = out;                      // start shifts at out
                while(in>0 && a[in-1].getR() >= temp) // until one is smaller,
                   {
                   a[in] = a[in-1];            // shift item to right
                   --in;                       // go left one position
                   }
                a[in] = t;                  // insert marked item
                }  
          break;
          case 2:
              for(out=1; out<nElems; out++)     // out is dividing line
                {
                int temp = a[out].getHR();            // remove marked item
                FantasyTeam t = a[out];
                in = out;                      // start shifts at out
                while(in>0 && a[in-1].getHR() >= temp) // until one is smaller,
                   {
                   a[in] = a[in-1];            // shift item to right
                   --in;                       // go left one position
                   }
                a[in] = t;                  // insert marked item
                }  
          break;
          case 3:
              for(out=1; out<nElems; out++)     // out is dividing line
                {
                int temp = a[out].getRBI();            // remove marked item
                FantasyTeam t = a[out];
                in = out;                      // start shifts at out
                while(in>0 && a[in-1].getRBI() >= temp) // until one is smaller,
                   {
                   a[in] = a[in-1];            // shift item to right
                   --in;                       // go left one position
                   }
                a[in] = t;                  // insert marked item
                }  
          break;
          case 4:
              for(out=1; out<nElems; out++)     // out is dividing line
                {
                int temp = a[out].getSB();            // remove marked item
                FantasyTeam t = a[out];
                in = out;                      // start shifts at out
                while(in>0 && a[in-1].getSB() >= temp) // until one is smaller,
                   {
                   a[in] = a[in-1];            // shift item to right
                   --in;                       // go left one position
                   }
                a[in] = t;                  // insert marked item
                }  
          break;
          case 5:
              for(out=1; out<nElems; out++)     // out is dividing line
                {
                double temp = a[out].getBA();            // remove marked item
                FantasyTeam t = a[out];
                in = out;                      // start shifts at out
                while(in>0 && a[in-1].getBA() >= temp) // until one is smaller,
                   {
                   a[in] = a[in-1];            // shift item to right
                   --in;                       // go left one position
                   }
                a[in] = t;                  // insert marked item
                }  
          break;
          case 6:
              for(out=1; out<nElems; out++)     // out is dividing line
                {
                double temp = a[out].getW();            // remove marked item
                FantasyTeam t = a[out];
                in = out;                      // start shifts at out
                while(in>0 && a[in-1].getW() >= temp) // until one is smaller,
                   {
                   a[in] = a[in-1];            // shift item to right
                   --in;                       // go left one position
                   }
                a[in] = t;                  // insert marked item
                }  
          break;
          case 7:
              for(out=1; out<nElems; out++)     // out is dividing line
                {
                int temp = a[out].getSV();            // remove marked item
                FantasyTeam t = a[out];
                in = out;                      // start shifts at out
                while(in>0 && a[in-1].getSV() >= temp) // until one is smaller,
                   {
                   a[in] = a[in-1];            // shift item to right
                   --in;                       // go left one position
                   }
                a[in] = t;                  // insert marked item
                }  
          break;
          case 8:
              for(out=1; out<nElems; out++)     // out is dividing line
                {
                int temp = a[out].getK();            // remove marked item
                FantasyTeam t = a[out];
                in = out;                      // start shifts at out
                while(in>0 && a[in-1].getK() >= temp) // until one is smaller,
                   {
                   a[in] = a[in-1];            // shift item to right
                   --in;                       // go left one position
                   }
                a[in] = t;                  // insert marked item
                }  
          break;
          case 9:
              for(out=1; out<nElems; out++)     // out is dividing line
                {
                double temp = a[out].getERA();            // remove marked item
                FantasyTeam t = a[out];
                in = out;                      // start shifts at out
                while(in>0 && a[in-1].getERA() >= temp) // until one is smaller,
                   {
                   a[in] = a[in-1];            // shift item to right
                   --in;                       // go left one position
                   }
                a[in] = t;                  // insert marked item
                }  
          break;
          case 10:
              for(out=1; out<nElems; out++)     // out is dividing line
                {
                double temp = a[out].getWHIP();            // remove marked item
                FantasyTeam t = a[out];
                in = out;                      // start shifts at out
                while(in>0 && a[in-1].getWHIP() >= temp) // until one is smaller,
                   {
                   a[in] = a[in-1];            // shift item to right
                   --in;                       // go left one position
                   }
                a[in] = t;                  // insert marked item
                }  
          break;
          default:
              for(out=1; out<nElems; out++)     // out is dividing line
                {
                double temp = a[out].getTotalPoints();            // remove marked item
                FantasyTeam t = a[out];
                in = out;                      // start shifts at out
                while(in>0 && a[in-1].getTotalPoints() >= temp) // until one is smaller,
                   {
                   a[in] = a[in-1];            // shift item to right
                   --in;                       // go left one position
                   }
                a[in] = t;                  // insert marked item
                }  
          break;
      }
      return a;
    }  // end insertionSort()
    private void sort(FantasyTeam [] a, int in, int out){
        for(out=1; out<a.length; out++)     // out is dividing line
            {
            int temp = a[out].getR();            // remove marked item
            FantasyTeam t = a[out];
            in = out;                      // start shifts at out
            while(in>0 && a[in-1].getR() >= temp) // until one is smaller,
               {
               a[in] = a[in-1];            // shift item to right
               --in;                       // go left one position
               }
            a[in] = t;                  // insert marked item
        }  
    }
//--------------------------------------------------------------
     
    public FantasyTeam getFreeAgent(){
        return freeAgent;
    }
    public void setDraftName(String name){
        draftName = name;
    }
    public String getDraftName(){
        return draftName;
    }
    public void addProTeam(String team){
        proTeams.add(team);
    }
    public void addPitcher(Pitcher pi){
        pitchers.add(pi);
    }
    public void addHitter(Hitter hi){
        hitters.add(hi);
    }
    public ObservableList<Hitter> getHitters(){
        return hitters;
    }
    public ObservableList<Pitcher> getPitchers(){
        return pitchers;
    }
    public ObservableList<String> getProTeams(){
        return proTeams;
    }
    /**
     * 
     * @param name
     * @param owner 
     */
    public void addFantasyTeam(String name, String owner){
        FantasyTeam team = new FantasyTeam(name, owner);
        fantasyTeams.add(team);
    }
    public void addFantasyTeam(FantasyTeam team){
        fantasyTeams.add(team);
    }
    public void removeFantasyTeam(FantasyTeam team){
        fantasyTeams.remove(team);
    }
    public ObservableList<FantasyTeam> getFantasyTeams(){
        return fantasyTeams;
    }
    public ObservableList<PlayerPresentor> getSetOfPlayers(String subs){
        ObservableList<PlayerPresentor> play = FXCollections.observableArrayList();
        for(int i = 0; i<players.size(); i++){
            String first = players.get(i).getFirstName(), last = players.get(i).getLastName();
            if(first.toLowerCase().startsWith(subs.toLowerCase())||last.toLowerCase().startsWith(subs.toLowerCase()))
                play.add(players.get(i));
        }
        return play;
    }
    public ObservableList<PlayerPresentor> getPosition(String subs, ObservableList<PlayerPresentor> player){
        if(subs.equalsIgnoreCase("All")){
            return players;
        }
        ObservableList<PlayerPresentor> play = FXCollections.observableArrayList();
        for(int i = 0; i<player.size(); i++){
            String position = player.get(i).getPosition();
            if(position.toLowerCase().contains(subs.toLowerCase()))
                play.add(player.get(i));
        }
        return play;
    }
    public ObservableList<PlayerPresentor> getPlayers(){
        if(players.size()>0)
            return players;
        else{
            int size = hitters.size()+pitchers.size();

            int hittersIndex = 0;
            int pitchersIndex = 0;
            PlayerPresentor player = null;
            int i=0;
            while( i<size){
                if(hittersIndex<hitters.size()){
                    int index = pitchers.size();
                    if(pitchersIndex == index)      
                player = new PlayerPresentor(pitchers.get(pitchersIndex-1),hitters.get(hittersIndex), 0);
                else
                  player = new PlayerPresentor(pitchers.get(pitchersIndex),hitters.get(hittersIndex), 0);
                    hittersIndex++;
                    players.add(player);
                i++;
                }
                if(pitchersIndex<pitchers.size()){
                    if(hittersIndex == hitters.size())
                player = new PlayerPresentor(pitchers.get(pitchersIndex), hitters.get(hittersIndex-1), 1);
                    else
                        player = new PlayerPresentor(pitchers.get(pitchersIndex), hitters.get(hittersIndex), 1);
                    pitchersIndex++;
                    players.add(player);
                i++;
                }
            }
        }
        return players;
    }   
   // public ObservableList

    public void addPlayerItem(PlayerPresentor player) {
        players.add(player);
    }
    public void removePlayerItem(PlayerPresentor player) {
        players.remove(player);
    }
    public ObservableList<PlayerPresentor> getProTeamsPlayers(String proTeam){
        ObservableList<PlayerPresentor> plays = FXCollections.observableArrayList();
        for(int j = 0; j<fantasyTeams.size(); j++) {
            ObservableList<PlayerPresentor> playerList = fantasyTeams.get(j).getPlayers();
            if(!(fantasyTeams.get(j).getName().equals("Free Agent"))){
                for(int i= 0; i<playerList.size(); i++){
                    if(playerList.get(i).getTeam()!=null){
                        if(playerList.get(i).getTeam().equals(proTeam)){
                            if(plays.contains(playerList.get(i))==false)
                            plays.add(playerList.get(i));
                        }
                    }
                }
                    
            }else{
               for(int i= 0; i<players.size(); i++){
                    if(players.get(i).getTeam().equals(proTeam)){
                        if(plays.contains(players.get(i))==false)
                        plays.add(players.get(i));
                    }
                }
                    
            }
        }
        return plays;
    }
    
}
class CompareByPoints implements Comparator{
    @Override
    public int compare(Object team1, Object team2) {
        if(((FantasyTeam)team1).getTotalPoints()>((FantasyTeam)team2).getTotalPoints()) {
            return 1;
        }else 
            return -1;  
    }
}
