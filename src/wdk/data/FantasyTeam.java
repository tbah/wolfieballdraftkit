/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package wdk.data;

import java.text.DecimalFormat;
import java.text.NumberFormat;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import javafx.beans.property.DoubleProperty;
import javafx.beans.property.IntegerProperty;
import javafx.beans.property.SimpleDoubleProperty;
import javafx.beans.property.SimpleIntegerProperty;
import javafx.beans.property.SimpleStringProperty;
import javafx.beans.property.StringProperty;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javax.swing.JOptionPane;

/**
 *
 * @author Thierno
 */
public class FantasyTeam {
    StringProperty name;
    StringProperty owner;
    IntegerProperty playerNeeded;
    DoubleProperty budjet;
    IntegerProperty pp;
    IntegerProperty r;
    IntegerProperty hr;
    IntegerProperty rbi;
    IntegerProperty sb;
    DoubleProperty ba;
    DoubleProperty w;
    IntegerProperty sv;
    IntegerProperty k;
    DoubleProperty era;
    DoubleProperty whip;
    DoubleProperty totalPoints;
    
    NumberFormat formatter;
    
    ObservableList<PlayerPresentor> players;
    ObservableList<PlayerPresentor> taxiSquad;
    ArrayList<String> positions;
    ArrayList<String> taxiPositions;
    int taxiPlayerNeeded;
    
    public FantasyTeam(String n, String o){
        String [] totalPositions = {"C","C","1B","3B","CI","2B","SS","MI",
                "OF","OF","OF","OF","OF","U","P","P","P","P","P","P","P","P",
                "P"};
        String [] taxiPosition = {"H","H","H","H","H","P","P","P"};
        formatter = new DecimalFormat("#0.00"); 
        name = new SimpleStringProperty();
        owner = new SimpleStringProperty();
        playerNeeded = new SimpleIntegerProperty();
        budjet = new SimpleDoubleProperty();
        pp = new SimpleIntegerProperty();
        r =  new SimpleIntegerProperty();
        hr =  new SimpleIntegerProperty();
        rbi =  new SimpleIntegerProperty();
        sb =  new SimpleIntegerProperty();
        sv =  new SimpleIntegerProperty();
        k =  new SimpleIntegerProperty();
        w =  new SimpleDoubleProperty();
        era =  new SimpleDoubleProperty();
        whip =  new SimpleDoubleProperty();
        ba = new SimpleDoubleProperty();
        budjet.set(260);
        playerNeeded.set(23);
        taxiPlayerNeeded = 8;
        int perPlayer = (int) (budjet.get()/playerNeeded.get());
        pp.set(perPlayer);
        
        totalPoints =  new SimpleDoubleProperty();
        name.set(n);
        owner.set(o);
        players = FXCollections.observableArrayList();
        taxiSquad = FXCollections.observableArrayList();
        positions = new ArrayList();
        taxiPositions = new ArrayList();
        for (int i = 0; i<totalPositions.length; i++){
            positions.add(totalPositions[i]);
            if(i<8)
             taxiPositions.add(taxiPosition[i]);
        }
        
        
    }
    /**
     * for free agent only
     * @param play 
     */
    public void setAll(ObservableList<PlayerPresentor> play){
        players = play;
        
    }
    public void addPlayerToTaxiSquad(PlayerPresentor player){
        if(taxiPlayerNeeded>0){
            taxiSquad.add(player);
            taxiPlayerNeeded--;
            taxiPositions.remove(player.getAssignedPosition());
        }   
        Collections.sort(taxiSquad, new ComparePositionTaxi());
    }
    public int getTaxiPlayerNeeded(){
        return taxiPlayerNeeded;
    }
    public ObservableList<PlayerPresentor> getTaxiSquad(){
        return taxiSquad;
    }
    private void upDateFantasyTeam(){
        int rCount = 0;
        int Hp = 0;
        int Hh = 0;
        int rbiCount = 0;
        double sbCount = 0;
        int ab = 0;
        double baCount = 0;
        double wCount = 0;
        int svCount = 0;
        int kCount = 0;
        double eraCount = 0;
        double whipCount = 0;
        int er = 0;
        double ip = 0;
        int hrCount = 0;
        
        for(int i=0; i<players.size(); i++){           
            if(!(players.get(i).getAssignedPosition().equals("P"))){
                sbCount +=players.get(i).getSBERA();
                rbiCount+=players.get(i).getRBIK();
                ab+=players.get(i).getAB();
                rCount += players.get(i).getRW();
                Hh +=players.get(i).getH();
                ab +=players.get(i).getAB();
                hrCount +=players.get(i).getHRSV();
            }else{
               wCount+= players.get(i).getRW();
               svCount+=players.get(i).getHRSV();
               kCount +=players.get(i).getRBIK();
               eraCount +=players.get(i).getSBERA();
               //er +=players.get(i).getER();
               Hp +=players.get(i).getH();
               er += players.get(i).getER();
               ip += players.get(i).getIP();
            }
        }
        if(ip!=0)
        whipCount = (wCount+Hp)/ip;
        else
            whipCount = ip;
        if(ip!=0)
        eraCount = (er*9)/ip;
        else
            eraCount = ip;
        if(ab!=0)
        baCount = (Hh/ab);
        else
            baCount = ab;
        double fomat = Double.parseDouble(formatter.format(whipCount));
        this.whip.set(fomat);
        fomat = Double.parseDouble(formatter.format(eraCount));
        this.era.set(fomat);
        fomat = Double.parseDouble(formatter.format(baCount));

        ba.set(fomat);
        k.set(kCount);
        r.set(rCount);
        hr.set(hrCount);
        rbi.set(rbiCount);
        sb.set((int)sbCount);
        w.set(wCount);
        sv.set(svCount);
        k.set(kCount);
        int val;
        if(playerNeeded.get()>0)
            val = (int)(budjet.get()/playerNeeded.get());
        else
            val = -1;
        
        pp.set(val);
    }
   
    public void addPlayer(PlayerPresentor p){
        if(playerNeeded.get()>0){ 
            String pos = p.getAssignedPosition();
            if(playerNeeded.get()<budjet.get()-p.getSalary()){
                 if(players.size()>0){
                      PlayerPresentor topPlay = players.get(0);
                      int index = 0;
                      while(index<players.size()){
                          topPlay = players.get(index);
                          if(getOrder(pos)<getOrder(topPlay.getAssignedPosition()))
                              break;
                              index++;
                      }
                      players.add(index, p);
                      playerNeeded.set(playerNeeded.get()-1);
                      budjet.set(budjet.get()-p.getSalary());
                      upDateFantasyTeam();
                      positions.remove(pos);
                 }else{
                     players.add(p);
                     playerNeeded.set(playerNeeded.get()-1);
                     budjet.set(budjet.get()-p.getSalary());
                     upDateFantasyTeam();
                     positions.remove(pos);
                 }

            }else{
                JOptionPane.showMessageDialog(null,"Your budget is limitted. \n"
                        + "You need "+playerNeeded.get()+" more players and your\n"
                        + "budget is "+budjet.get()+", you can't spend more than\n"
                        + ""+(budjet.get()-playerNeeded.get()-1)+"$ on this player");
            }
        }else{
            p.setContract("X");
            p.setSalary(1);
            addPlayerToTaxiSquad(p);
        }
    }
    public void removePlayer(PlayerPresentor p){
        players.remove(p);
        playerNeeded.set(playerNeeded.get()+1);
        budjet.set(budjet.get()+p.getSalary());
        upDateFantasyTeam();
        String pos = p.getAssignedPosition();
        positions.add(pos);
    }
    public ArrayList<String> getNeededPostion(){
        return positions;
    }
     public String getName(){
        return name.get();
    }
    public StringProperty getNameProperty(){
        return name;
    }
    public StringProperty getOwnerProperty(){
        return owner;
    }
    public String getOwner(){
        return owner.get();
    }

    public void setName(String newValue) {
        name.set(newValue);
    }
    public void setOwner(String newValue) {
        owner.set(newValue);
    }
    @Override
    public String toString(){
        return name.get();
    }
    
    public IntegerProperty playerNeededProperty(){
        return playerNeeded;
    }
    public int getPlayerNeeded(){
        return playerNeeded.get();
    }
    public void setPlayerNeeded(int n){
        playerNeeded.set(n);
    }
    public DoubleProperty budjetProperty(){
        return budjet;
    }
    public double getBudjet(){
        return budjet.get(); // budget
    }
    public IntegerProperty ppProperty(){
        return pp;
    }
    public int getPP(){
        return pp.get();
    }
    public IntegerProperty rProperty(){
        return r;
    }
    public int getR(){
        return r.get();
    }
    public IntegerProperty hrProperty(){
        return hr;
    }
    public int getHR(){
        return hr.get();
    }
    public IntegerProperty rbiProperty(){
        return rbi;
    }
    public int getRBI(){
        return rbi.get();
    }
    public IntegerProperty sbProperty(){
        return sb;
    }
    public int getSB(){
        return sb.get();
    }
    public DoubleProperty baProperty(){
        return ba;
    }
    public double getBA(){
        return ba.get();
    }
    public DoubleProperty wProperty(){
        return w;
    }
    public double getW(){
        return w.get();
    }
    public IntegerProperty svProperty(){
        return sv;
    }
    public int getSV(){
        return sv.get();
    }
    public IntegerProperty kProperty(){
        return k;
    }
    public int getK(){
        return k.get();
    }
    public DoubleProperty eraProperty(){
        return era;
    }
    public double getERA(){
        return era.get();
    }
    public DoubleProperty whipProperty(){
        return whip;
    }
    public double getWHIP(){
        return whip.get();
    }
    public void setTotalPoints(double points){
        totalPoints.set(points);
    }
    public void addPoint(double point){
        double current = totalPoints.get();
        current += point;
        totalPoints.set(current);
    }
    public void removePoint(double point){
        double current = totalPoints.get();
        current -= point;
        totalPoints.set(current);
    }
    public DoubleProperty totalPointsProperty(){
        return totalPoints;
    }
    public double getTotalPoints(){
        return totalPoints.get();
    }
    public ObservableList<String> getTaxiPosition(PlayerPresentor player){
        char pos = player.getPosition().charAt(0);
        ObservableList<String> positionsToreturn = FXCollections.observableArrayList();
        for(int i = 0; i<taxiPositions.size();i++){
             if(pos =='P'){
                 if(taxiPositions.get(i).equals("P")){
                    positionsToreturn.add("P");
                    break;
                 }
             }
             else{
                 if(taxiPositions.get(i).equals("H")){
                    positionsToreturn.add("H"); 
                    break;
                 }
             }
        }
        return positionsToreturn;
    }
    public ObservableList<String> getAvailablePosPlayer(PlayerPresentor play){
         String [] pos;
         
         pos = play.getPosition().split("_");
         
         ObservableList<String> positionsToreturn = FXCollections.observableArrayList();
         for(int i = 0; i<positions.size();i++){
             for(int j=0; j<pos.length; j++){
                 if(positions.get(i).equals(pos[j])){
                     if(positionsToreturn.contains(pos[j])==false)
                       positionsToreturn.add(pos[j]);
                 }
             }
            if(positions.get(i).equals("CI")){
                for(int j=0; j<pos.length; j++){
                 if(pos[j].equals("1B")||pos[j].equals("3B")){
                     if(positionsToreturn.contains(positions.get(i))==false)
                       positionsToreturn.add(positions.get(i));
                 }
               }   
            }
            if(positions.get(i).equals("MI")){
                for(int j=0; j<pos.length; j++){
                 if(pos[j].equals("2B")||pos[j].equals("SS")){
                     if(positionsToreturn.contains(positions.get(i))==false)
                       positionsToreturn.add(positions.get(i));
                 }
               }   
            }
            if(positions.get(i).equals("U")){
                for(int j=0; j<pos.length; j++){
                 if(!(pos[j].equals("P"))){
                     if(positionsToreturn.contains(positions.get(i))==false)
                       positionsToreturn.add(positions.get(i));
                 }
               }   
            }
         }
         return positionsToreturn;
    }
    public ObservableList<PlayerPresentor> getPlayers(){
        return players;
    }
    private int getOrder(String pos){
        String [] order =  {"C","1B","CI","3B","2B","MI","SS",
                "OF","U","P"};
        int ord = 0;
        for( ; ord<order.length; ord++){
            if(order[ord].equals(pos))
                return ord;
        }
                return ord;
    }
    public void setBudjet(double num){
        budjet.set(num);
    }
    public void setPP(int num){
        pp.set(num);
    }
    public void setR(int num){
        r.set(num);
    }
    public void setHR(int num){
        hr.set(num);
    }
    public void setRBI(int num){
        rbi.set(num);
    }
    public void setSB(int num){
        sb.set(num);
    }
    public void setBA(double num){
        ba.set(num);
    }
    public void setW(double num){
        w.set(num);
    }
    public void setSV(int num){
        sv.set(num);
    }
    public void setK(int num){
        k.set(num);
    }
    public void setERA(double num){
        era.set(num);
    }
    public void setWHIP(double num){
        budjet.set(num);
    }
 }
class ComparePositionTaxi implements Comparator{
    @Override
    public int compare(Object player1, Object player2) {
      if(((PlayerPresentor)player1).getAssignedPosition().charAt(0)>((PlayerPresentor)player2).getAssignedPosition().charAt(0))            
          return 1;
      else
          return -1;
    }   
    
}



