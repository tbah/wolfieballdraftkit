/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package wdk.data;

import javafx.beans.property.DoubleProperty;
import javafx.beans.property.IntegerProperty;
import javafx.beans.property.SimpleDoubleProperty;
import javafx.beans.property.SimpleIntegerProperty;

/**
 *
 * @author Thierno
 */
public class Hitter extends Player {
    boolean isHitter = true;
    IntegerProperty ab;
    IntegerProperty r;
    IntegerProperty h;
    IntegerProperty hr;
    IntegerProperty rbi;
    DoubleProperty sb;
    DoubleProperty ba;
    public Hitter(){
        super();
       ab = new SimpleIntegerProperty();
       r = new SimpleIntegerProperty();
       h = new SimpleIntegerProperty();
       hr = new SimpleIntegerProperty();
       rbi = new SimpleIntegerProperty();
       sb = new SimpleDoubleProperty();
       ba = new SimpleDoubleProperty();
    }
     public void setAB(int num){
        ab.set(num);
    }
    public void setR(int num){
        r .set(num);
        super.rw.set(num);
    }
    public void setHR(int num){
        hr.set(num);
        super.hrsv.set(num);
    }
    public void setH(int num){
        h.set(num);
       // super
    }
    public void setRBI(int num){
        rbi.set(num);
        super.rbik.set(num);
    }
    public void setSB(int num){
        sb.set(num);
        super.sbera.set((double)num);
    }
    public int getAB(){
        return ab.get();
    }
    public int getR(){
        return r.get();
    }
    /*@Override
    public Integer getRW(){
        return super.getRW();
    }
    @Override
    public IntegerProperty rwProperty(){
        return super.rwProperty();
    }*/
    
    public int getHR(){
        return hr.get();
    }
    public int getH(){
        return h.get();
    }
    public int getRBI(){
        return rbi.get();
    }
    public int getSB(){
        return (int) sb.get();
    }
    public IntegerProperty getABProperty(){
        return ab;
    }
    public IntegerProperty getRProperty(){
        return r;
    }
    public IntegerProperty getHRProperty(){
        return hr;
    }
    public IntegerProperty getHProperty(){
        return h;
    }
    public IntegerProperty getRBIProperty(){
        return rbi;
    }
    public DoubleProperty getSBProperty(){
        return sb;
    }
    public double getBA(){
       double temp = 0.0;
        if(ab.get()>0)
        temp = h.get()/ab.get();
        //super.bawhip.set(ba.get());
        ba.set(temp);
        return ba.get();
    }
    @Override
    public DoubleProperty bawhipProperty(){
        double temp = getBA();
        super.bawhip.set(temp);
        return ba;
    }
    @Override
    public Double getBAWHIP(){
        getBA();
        return ba.get();
    }
    public DoubleProperty getBAProperty(){
        getBA();
        return ba;
    }
}
