/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package wdk.data;

import wdk.file.DraftFileManager;

/**
 *
 * @author Thierno
 */
public class DraftDataManager {

    DraftDataView view;
    Draft draft;
    DraftFileManager draftFileManager;
     public DraftDataManager(DraftDataView view){
         this.view = view;
         draft = new Draft();
     }
    public Draft getDraft() {
        return draft;
    }

    public void reset() {
        
        view.reloadDraft(draft);
    }
    public DraftFileManager getFileManager() {
        return draftFileManager;
    }
}
